#ifndef TICTACTOE_H
#define TICTACTOE_H

#include <stdbool.h>

#define BOARD_SIZE 3

// Structure pour représenter une case du plateau
typedef struct {
    char symbol; // Contient 'X', 'O' ou un caractère vide ('.')
    bool occupied; // Indique si la case est occupée ou non
} Cell;

// Fonction pour initialiser le plateau avec des cases vides
void initializeBoard(Cell board[BOARD_SIZE][BOARD_SIZE]);

// Fonction pour afficher le plateau de jeu
void displayBoard(Cell board[BOARD_SIZE][BOARD_SIZE]);

// Fonction pour vérifier si le plateau est plein (match nul)
bool isBoardFull(Cell board[BOARD_SIZE][BOARD_SIZE]);

// Fonction pour vérifier si un joueur a gagné la partie
bool checkWin(Cell board[BOARD_SIZE][BOARD_SIZE], char player);

// Fonction pour placer le symbole d'un joueur sur le plateau
void placeSymbol(Cell board[BOARD_SIZE][BOARD_SIZE], int row, int col, char player);

#endif // TICTACTOE_H
