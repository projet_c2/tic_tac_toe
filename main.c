#include <stdio.h>
#include <stdbool.h>
#include "tictactoe.h"
#include "qlearning.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

// ... (Déclarations et autres fonctions)

// Fonction pour afficher le plateau de jeu en utilisant SDL
void displayBoardSDL(Cell board[BOARD_SIZE][BOARD_SIZE], SDL_Renderer* renderer, TTF_Font* font) {
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255); // Couleur de fond (blanc)
    SDL_RenderClear(renderer);

    // Affichage du plateau
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            SDL_Rect rect = { j * 100, i * 100, 100, 100 };
            SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255); // Couleur des lignes (noir)
            SDL_RenderDrawRect(renderer, &rect);

            if (board[i][j].symbol == 'X') {
                // Afficher "X"
                SDL_Surface* surface = TTF_RenderText_Solid(font, "X", SDL_Color{ 0, 0, 0, 255 });
                SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
                SDL_RenderCopy(renderer, texture, NULL, &rect);
                SDL_DestroyTexture(texture);
                SDL_FreeSurface(surface);
            }
            else if (board[i][j].symbol == 'O') {
                // Afficher "O"
                SDL_Surface* surface = TTF_RenderText_Solid(font, "O", SDL_Color{ 0, 0, 0, 255 });
                SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
                SDL_RenderCopy(renderer, texture, NULL, &rect);
                SDL_DestroyTexture(texture);
                SDL_FreeSurface(surface);
            }
        }
    }

    SDL_RenderPresent(renderer);
}

// Fonction pour permettre à l'IA de jouer contre un joueur humain en utilisant SDL
void playAgainstAI(SDL_Renderer* renderer, TTF_Font* font, double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE]) {
    Cell board[BOARD_SIZE][BOARD_SIZE];
    initializeBoard(board);

    char currentPlayer = 'X';
    bool gameWon = false;

    printf("Bienvenue dans le jeu Tic-Tac-Toe contre l'IA entraînée !\n");
    printf("Vous jouez en tant que X. Entrez le numéro de ligne (0-2) et le numéro de colonne (0-2) pour placer votre symbole.\n");

    while (!gameWon && !isBoardFull(board)) {
        // Affichage du plateau de jeu actuel
        displayBoardSDL(board, renderer, font);

        // ... (Le reste de la logique de jeu reste inchangé comme dans la version précédente)
    }
}

int main() {
    // Initialisation de SDL
    SDL_Init(SDL_INIT_VIDEO);
    TTF_Init();

    // Création de la fenêtre et du renderer pour l'affichage
    SDL_Window* window = SDL_CreateWindow("Tic-Tac-Toe", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 300, 300, SDL_WINDOW_SHOWN);
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    // Chargement de la police pour l'affichage du texte
    TTF_Font* font = TTF_OpenFont("path/to/your/font.ttf", 60); // Remplacez "path/to/your/font.ttf" par le chemin vers votre police

    // Appel de la fonction pour entraîner l'IA
    double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE] = { 0 };
    trainAI(Q);

    // Appel de la fonction pour jouer contre l'IA entraînée
    playAgainstAI(renderer, font, Q);

    // Nettoyage et fermeture de SDL
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    TTF_CloseFont(font);
    TTF_Quit();
    SDL_Quit();

    return 0;
}




/* bizzare    
 #include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

#define BOARD_SIZE 3
#define MAX_EPISODES 10000
#define LEARNING_RATE 0.1
#define DISCOUNT_FACTOR 0.9
#define EXPLORATION_PROB 0.3

// Structure pour représenter une case du plateau
typedef struct {
    char symbol; // Contient 'X', 'O' ou un caractère vide ('.')
    bool occupied; // Indique si la case est occupée ou non
} Cell;

// Fonction pour initialiser le plateau avec des cases vides
void initializeBoard(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            board[i][j].symbol = '.';
            board[i][j].occupied = false;
        }
    }
}

// Fonction pour afficher le plateau de jeu
void displayBoard(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            printf(" %c ", board[i][j].symbol);
            if (j < BOARD_SIZE - 1)
                printf("|");
        }
        printf("\n");
        if (i < BOARD_SIZE - 1)
            printf("-----------\n");
    }
}

// Fonction pour vérifier si le plateau est plein (match nul)
bool isBoardFull(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            if (!board[i][j].occupied)
                return false;
        }
    }
    return true;
}

// Fonction pour vérifier si un joueur a gagné la partie
bool checkWin(Cell board[BOARD_SIZE][BOARD_SIZE], char player) {
    // Vérification des lignes et des colonnes
    for (int i = 0; i < BOARD_SIZE; i++) {
        if ((board[i][0].symbol == player && board[i][1].symbol == player && board[i][2].symbol == player) ||
            (board[0][i].symbol == player && board[1][i].symbol == player && board[2][i].symbol == player)) {
            return true;
        }
    }
    // Vérification des diagonales
    if ((board[0][0].symbol == player && board[1][1].symbol == player && board[2][2].symbol == player) ||
        (board[0][2].symbol == player && board[1][1].symbol == player && board[2][0].symbol == player)) {
        return true;
    }
    return false;
}

// Fonction pour placer le symbole d'un joueur sur le plateau
void placeSymbol(Cell board[BOARD_SIZE][BOARD_SIZE], int row, int col, char player) {
    board[row][col].symbol = player;
    board[row][col].occupied = true;
}

// Fonction pour choisir la meilleure action (numéro de case) en fonction des valeurs Q
int chooseAction(Cell board[BOARD_SIZE][BOARD_SIZE], char player, double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE]) {
    // Exploration aléatoire avec une certaine probabilité
    if ((double)rand() / RAND_MAX < EXPLORATION_PROB) {
        int emptyCells[BOARD_SIZE * BOARD_SIZE];
        int emptyCellCount = 0;
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                if (!board[i][j].occupied) {
                    emptyCells[emptyCellCount] = i * BOARD_SIZE + j;
                    emptyCellCount++;
                }
            }
        }
        return emptyCells[rand() % emptyCellCount];
    } else {
        // Exploitation : choisir la meilleure action selon les valeurs Q
        int maxAction = -1;
        double maxQ = -1;
        for (int i = 0; i < BOARD_SIZE * BOARD_SIZE; i++) {
            if (!board[i / BOARD_SIZE][i % BOARD_SIZE].occupied) {
                if (Q[i][player - 'X'] > maxQ) {
                    maxQ = Q[i][player - 'X'];
                    maxAction = i;
                }
            }
        }
        return maxAction;
    }
}

// Fonction pour entraîner l'IA en utilisant l'algorithme Q-learning
void trainAI() {
    Cell board[BOARD_SIZE][BOARD_SIZE];
    initializeBoard(board);

    double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE] = {0}; // Tableau des valeurs Q

    // Initialiser la graine aléatoire pour la sélection d'actions aléatoires
    srand(time(NULL));

    for (int episode = 0; episode < MAX_EPISODES; episode++) {
        // Initialisation du jeu
        initializeBoard(board);
        char currentPlayer = 'X';
        bool gameWon = false;

        while (!gameWon && !isBoardFull(board)) {
            int action = chooseAction(board, currentPlayer, Q);

            int row = action / BOARD_SIZE;
            int col = action % BOARD_SIZE;

            placeSymbol(board, row, col, currentPlayer);

            if (checkWin(board, currentPlayer)) {
                gameWon = true;
                // Récompense +1 pour la victoire
                Q[action][currentPlayer - 'X'] += LEARNING_RATE * (1 - Q[action][currentPlayer - 'X']);
            } else if (isBoardFull(board)) {
                // Récompense 0 pour un match nul
                Q[action][currentPlayer - 'X'] += LEARNING_RATE * (0 - Q[action][currentPlayer - 'X']);
            } else {
                // Le jeu continue
                char nextPlayer = (currentPlayer == 'X') ? 'O' : 'X';
                int nextAction = chooseAction(board, nextPlayer, Q);
                // Mise à jour de la valeur Q avec l'algorithme Q-learning
                Q[action][currentPlayer - 'X'] += LEARNING_RATE * (DISCOUNT_FACTOR * Q[nextAction][nextPlayer - 'X'] - Q[action][currentPlayer - 'X']);
            }

            currentPlayer = (currentPlayer == 'X') ? 'O' : 'X'; // Changer de joueur
        }
    }

    // Après l'entraînement, vous pouvez enregistrer le tableau de valeurs Q pour une utilisation ultérieure
    // (par exemple, pour jouer contre l'IA entraînée).
}

// Fonction pour permettre à l'IA de jouer contre un joueur humain
void playAgainstAI(double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE]) {
    Cell board[BOARD_SIZE][BOARD_SIZE];
    initializeBoard(board);

    char currentPlayer = 'X';
    bool gameWon = false;

    printf("Bienvenue dans le jeu Tic-Tac-Toe contre l'IA entraînée !\n");
    printf("Vous jouez en tant que X. Entrez le numéro de ligne (0-2) et le numéro de colonne (0-2) pour placer votre symbole.\n");

    while (!gameWon && !isBoardFull(board)) {
        // Affichage du plateau de jeu actuel
        displayBoard(board);

        if (currentPlayer == 'X') {
            // Tour du joueur humain
            int row, col;
            printf("C'est à votre tour. Entrez le numéro de ligne : ");
            scanf("%d", &row);
            printf("Entrez le numéro de colonne : ");
            scanf("%d", &col);

            // Vérification de la validité du mouvement
            if (row >= 0 && row < BOARD_SIZE && col >= 0 && col < BOARD_SIZE && !board[row][col].occupied) {
                placeSymbol(board, row, col, currentPlayer);

                if (checkWin(board, currentPlayer)) {
                    gameWon = true;
                    printf("Félicitations ! Vous avez gagné contre l'IA.\n");
                } else {
                    currentPlayer = 'O'; // Passage à l'IA pour son tour
                }
            } else {
                printf("Mouvement invalide. Réessayez.\n");
            }
        } else {
            // Tour de l'IA
            int action = chooseAction(board, currentPlayer, Q);

            int row = action / BOARD_SIZE;
            int col = action % BOARD_SIZE;

            printf("L'IA joue : ligne %d, colonne %d\n", row, col);
            placeSymbol(board, row, col, currentPlayer);

            if (checkWin(board, currentPlayer)) {
                gameWon = true;
                printf("Dommage ! L'IA a gagné cette fois.\n");
            } else {
                currentPlayer = 'X'; // Passage au joueur humain pour son tour
            }
        }
    }

    // Affichage du plateau final
    displayBoard(board);

    if (!gameWon) {
        printf("Partie terminée. Match nul !\n");
    }
}

int main() {
    // Appel de la fonction pour entraîner l'IA
    double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE] = {0};
    trainAI(Q);

    // Appel de la fonction pour jouer contre l'IA entraînée
    playAgainstAI(Q);

    return 0;
}

*/


/*Avec cette modification, le joueur humain peut désormais cliquer sur une case pour placer son symbole 'X'. L'IA continuera de choisir ses actions en utilisant l'algorithme Q-learning, mais le joueur aura désormais un moyen plus interactif de jouer contre l'IA.*/

#include <SDL2/SDL.h>

// Fonction pour obtenir le mouvement de l'utilisateur en cliquant sur une case
int getUserMove(SDL_Event event) {
    if (event.type == SDL_MOUSEBUTTONDOWN) {
        int mouseX, mouseY;
        SDL_GetMouseState(&mouseX, &mouseY);

        // Calculer la case correspondante en fonction de la position de la souris
        int row = mouseY / (WINDOW_HEIGHT / BOARD_SIZE);
        int col = mouseX / (WINDOW_WIDTH / BOARD_SIZE);

        return row * BOARD_SIZE + col;
    }
    return -1; // Retourne -1 si aucun clic n'a été effectué
}

void playAgainstAI(double Q[BOARD_SIZE * BOARD_SIZE][NUM_DISCRETE_STATES], SDL_Renderer* renderer, TTF_Font* font) {
    Cell board[BOARD_SIZE][BOARD_SIZE];
    initializeBoard(board);

    char currentPlayer = 'X';
    bool gameWon = false;

    printf("Bienvenue dans le jeu Tic-Tac-Toe contre l'IA entraînée !\n");
    printf("Vous jouez en tant que X. Cliquez sur une case pour placer votre symbole.\n");

    SDL_Event event; // Variable pour stocker les événements SDL

    while (!gameWon && !isBoardFull(board)) {
        // Affichage du plateau de jeu actuel
        displayBoardSDL(board, renderer, font);

        if (currentPlayer == 'X') {
            // Tour du joueur humain
            int move = -1;

            while (move == -1) {
                SDL_WaitEvent(&event); // Attendre un événement

                if (event.type == SDL_QUIT) {
                    // Arrêter le jeu si l'utilisateur ferme la fenêtre
                    return;
                }

                move = getUserMove(event);
            }

            // Vérification de la validité du mouvement
            if (!board[move / BOARD_SIZE][move % BOARD_SIZE].occupied) {
                placeSymbol(board, move / BOARD_SIZE, move % BOARD_SIZE, currentPlayer);

                if (checkWin(board, currentPlayer)) {
                    gameWon = true;
                    printf("Félicitations ! Vous avez gagné contre l'IA.\n");
                } else {
                    currentPlayer = 'O'; // Passage à l'IA pour son tour
                }
            }
        } else {
            // Tour de l'IA
            int action = chooseAction(board, currentPlayer, Q);

            int row = action / BOARD_SIZE;
            int col = action % BOARD_SIZE;

            printf("L'IA joue : ligne %d, colonne %d\n", row, col);
            placeSymbol(board, row, col, currentPlayer);

            if (checkWin(board, currentPlayer)) {
                gameWon = true;
                printf("Dommage ! L'IA a gagné cette fois.\n");
            } else {
                currentPlayer = 'X'; // Passage au joueur humain pour son tour
            }
        }
    }

    // Affichage du plateau final
    displayBoardSDL(board, renderer, font);
    SDL_Delay(3000);

    if (!gameWon) {
        printf("Partie terminée. Match nul !\n");
    }
}

//// et cela aussi 
#include <SDL2/SDL_ttf.h>

// Définition des dimensions de la fenêtre du jeu
#define WINDOW_WIDTH 300
#define WINDOW_HEIGHT 300

// Définition des dimensions des cases du plateau de jeu
#define CELL_WIDTH (WINDOW_WIDTH / BOARD_SIZE)
#define CELL_HEIGHT (WINDOW_HEIGHT / BOARD_SIZE)

// Fonction pour afficher le plateau de jeu avec les symboles 'X' et 'O'
void displayBoardSDL(Cell board[BOARD_SIZE][BOARD_SIZE], SDL_Renderer* renderer, TTF_Font* font) {
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255); // Couleur de fond (blanc)
    SDL_RenderClear(renderer); // Effacer le rendu précédent

    // Parcourir le plateau de jeu et afficher les symboles 'X' et 'O'
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            // Coordonnées du coin supérieur gauche de la case
            int x = j * CELL_WIDTH;
            int y = i * CELL_HEIGHT;

            // Dessiner le contour de la case
            SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
            SDL_Rect cellRect = {x, y, CELL_WIDTH, CELL_HEIGHT};
            SDL_RenderDrawRect(renderer, &cellRect);

            // Afficher le symbole 'X' ou 'O' s'il y a un joueur dans la case
            if (board[i][j].symbol == 'X') {
                SDL_Color color = {0, 0, 0, 255}; // Couleur du symbole 'X' (noir)
                drawText(renderer, font, "X", x + CELL_WIDTH / 2 - 10, y + CELL_HEIGHT / 2 - 10, color);
            } else if (board[i][j].symbol == 'O') {
                SDL_Color color = {255, 0, 0, 255}; // Couleur du symbole 'O' (rouge)
                drawText(renderer, font, "O", x + CELL_WIDTH / 2 - 10, y + CELL_HEIGHT / 2 - 10, color);
            }
        }
    }

    SDL_RenderPresent(renderer); // Afficher le rendu à l'écran
}

// Fonction pour dessiner du texte sur le rendu
void drawText(SDL_Renderer* renderer, TTF_Font* font, const char* text, int x, int y, SDL_Color color) {
    SDL_Surface* surface = TTF_RenderText_Solid(font, text, color);
    SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);

    int width = 0;
    int height = 0;
    SDL_QueryTexture(texture, NULL, NULL, &width, &height);

    SDL_Rect dstRect = {x, y, width, height};
    SDL_RenderCopy(renderer, texture, NULL, &dstRect);

    SDL_DestroyTexture(texture);
    SDL_FreeSurface(surface);
}
/////


#include <stdio.h>
#include <stdbool.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include "tictactoe.h"
#include "qlearning.h"

// ...

void playAgainstAI(double Q[BOARD_SIZE * BOARD_SIZE][NUM_DISCRETE_STATES], SDL_Renderer* renderer, TTF_Font* font) {
    // ... (Le reste du code de la fonction playAgainstAI comme dans la version précédente)

    // Boucle pour demander au joueur s'il souhaite continuer à jouer
    bool playAgain = true;
    while (playAgain) {
        // Affichage du plateau final
        displayBoardSDL(board, renderer, font);
        SDL_Delay(3000);

        if (!gameWon) {
            printf("Partie terminée. Match nul !\n");
        }

        // Demander au joueur s'il veut continuer
        printf("Voulez-vous jouer à nouveau ? (o/n) : ");
        char response;
        scanf(" %c", &response);
        if (response != 'o' && response != 'O') {
            playAgain = false;
        } else {
            // Réinitialiser le plateau pour une nouvelle partie
            initializeBoard(board);
            currentPlayer = 'X';
            gameWon = false;
        }
    }
}
////

#include <stdio.h>
#include <stdbool.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include "tictactoe.h"
#include "qlearning.h"

// ...

// Fonction pour afficher le plateau de jeu et gérer les événements SDL
bool displayBoardSDL(Cell board[BOARD_SIZE][BOARD_SIZE], SDL_Renderer* renderer, TTF_Font* font) {
    // ... (Le reste du code de la fonction displayBoardSDL comme dans la version précédente)

    if (gameWon || isBoardFull(board)) {
        // Afficher la boîte de dialogue pour demander au joueur s'il veut continuer
        SDL_Color textColor = {0, 0, 0};
        SDL_Surface* surfaceMessage;
        SDL_Texture* messageTexture;
        char message[100];

        snprintf(message, 100, "Voulez-vous jouer à nouveau ? (o/n) : ");
        surfaceMessage = TTF_RenderText_Solid(font, message, textColor);
        messageTexture = SDL_CreateTextureFromSurface(renderer, surfaceMessage);

        int textWidth, textHeight;
        SDL_QueryTexture(messageTexture, NULL, NULL, &textWidth, &textHeight);

        int x = (SCREEN_WIDTH - textWidth) / 2;
        int y = (SCREEN_HEIGHT - textHeight) / 2;

        SDL_Rect messageRect = {x, y, textWidth, textHeight};
        SDL_RenderCopy(renderer, messageTexture, NULL, &messageRect);

        SDL_FreeSurface(surfaceMessage);
        SDL_DestroyTexture(messageTexture);

        SDL_RenderPresent(renderer);

        SDL_Event event;
        bool responseReceived = false;

        while (!responseReceived) {
            while (SDL_PollEvent(&event) != 0) {
                if (event.type == SDL_QUIT) {
                    running = false;
                    responseReceived = true;
                } else if (event.type == SDL_KEYDOWN) {
                    if (event.key.keysym.sym == SDLK_o || event.key.keysym.sym == SDLK_O) {
                        // Le joueur veut continuer à jouer
                        initializeBoard(board);
                        currentPlayer = 'X';
                        gameWon = false;
                        responseReceived = true;
                    } else if (event.key.keysym.sym == SDLK_n || event.key.keysym.sym == SDLK_N) {
                        // Le joueur ne veut pas continuer
                        running = false;
                        responseReceived = true;
                    }
                }
            }
        }
    }

    // ... (Le reste du code de la fonction displayBoardSDL comme dans la version précédente)
}

/*   important  ******************** */
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include "tictactoe.h"

// Structure pour représenter une règle
typedef struct {
    int (*ruleFunction)(Cell board[BOARD_SIZE][BOARD_SIZE], char player); // Pointeur vers la fonction de la règle
    double priority; // Priorité de la règle (modifiable par l'IA)
} Rule;

// Liste de toutes les règles
Rule rules[NUM_RULES];

// Fonction pour initialiser les règles avec des valeurs par défaut
void initializeRules() {
    // Règles basées sur les lignes
    rules[0].ruleFunction = checkRowCompletion;
    rules[0].priority = 1.0;
    rules[1].ruleFunction = blockOpponentRow;
    rules[1].priority = 1.0;

    // Règles basées sur les colonnes
    rules[2].ruleFunction = checkColumnCompletion;
    rules[2].priority = 1.0;
    rules[3].ruleFunction = blockOpponentColumn;
    rules[3].priority = 1.0;

    // Règles basées sur les diagonales
    rules[4].ruleFunction = checkDiagonalCompletion;
    rules[4].priority = 1.0;
    rules[5].ruleFunction = blockOpponentDiagonal;
    rules[5].priority = 1.0;

    // Règles basées sur les possibilités de victoire
    rules[6].ruleFunction = checkPotentialWin;
    rules[6].priority = 1.0;
    rules[7].ruleFunction = blockPotentialWin;
    rules[7].priority = 1.0;
}

// Fonction pour choisir la meilleure action (numéro de case) en fonction des règles et des valeurs Q
int chooseAction(Cell board[BOARD_SIZE][BOARD_SIZE], char player, double Q[BOARD_SIZE * BOARD_SIZE][NUM_DISCRETE_STATES]) {
    int usedRuleIndices[NUM_RULES];
    int numUsedRules = 0;

    // Parcourir toutes les règles pour vérifier lesquelles sont utilisées par l'IA dans la partie actuelle
    for (int i = 0; i < NUM_RULES; i++) {
        if (rules[i].ruleFunction(board, player) >= 0) {
            usedRuleIndices[numUsedRules] = i;
            numUsedRules++;
        }
    }

    if (numUsedRules > 0) {
        // Sélectionner la règle avec la plus haute priorité
        int maxPriorityIndex = usedRuleIndices[0];
        double maxPriority = rules[maxPriorityIndex].priority;

        for (int i = 1; i < numUsedRules; i++) {
            int ruleIndex = usedRuleIndices[i];
            double priority = rules[ruleIndex].priority;
            if (priority > maxPriority) {
                maxPriority = priority;
                maxPriorityIndex = ruleIndex;
            }
        }

        // Appliquer la règle avec la plus haute priorité
        int action = rules[maxPriorityIndex].ruleFunction(board, player);
        if (action >= 0) {
            return action;
        }
    }

    // Si aucune règle n'est applicable, effectuer une exploration aléatoire avec une certaine probabilité
    int emptyCells[BOARD_SIZE * BOARD_SIZE];
    int emptyCellCount = 0;
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            if (!board[i][j].occupied) {
                emptyCells[emptyCellCount] = i * BOARD_SIZE + j;
                emptyCellCount++;
            }
        }
    }

    return emptyCells[rand() % emptyCellCount];
}

// Fonction pour entraîner l'IA en utilisant l'algorithme Q-learning
void trainAI() {
    // ... (Le reste du code de la fonction trainAI() comme dans la version précédente)
}
 
 
 
 
 /* les regles */
 // Fonction pour vérifier si une ligne est complétée par le joueur
int checkRowCompletion(Cell board[BOARD_SIZE][BOARD_SIZE], char player) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        if (board[i][0].symbol == player && board[i][1].symbol == player && board[i][2].symbol == ' ') {
            return i * BOARD_SIZE + 2;
        } else if (board[i][0].symbol == player && board[i][1].symbol == ' ' && board[i][2].symbol == player) {
            return i * BOARD_SIZE + 1;
        } else if (board[i][0].symbol == ' ' && board[i][1].symbol == player && board[i][2].symbol == player) {
            return i * BOARD_SIZE;
        }
    }
    return -1;
}

// Fonction pour bloquer une ligne complétée par l'adversaire
int blockOpponentRow(Cell board[BOARD_SIZE][BOARD_SIZE], char player) {
    char opponent = (player == 'X') ? 'O' : 'X';
    for (int i = 0; i < BOARD_SIZE; i++) {
        if (board[i][0].symbol == opponent && board[i][1].symbol == opponent && board[i][2].symbol == ' ') {
            return i * BOARD_SIZE + 2;
        } else if (board[i][0].symbol == opponent && board[i][1].symbol == ' ' && board[i][2].symbol == opponent) {
            return i * BOARD_SIZE + 1;
        } else if (board[i][0].symbol == ' ' && board[i][1].symbol == opponent && board[i][2].symbol == opponent) {
            return i * BOARD_SIZE;
        }
    }
    return -1;
}

// Fonction pour vérifier si une colonne est complétée par le joueur
int checkColumnCompletion(Cell board[BOARD_SIZE][BOARD_SIZE], char player) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        if (board[0][i].symbol == player && board[1][i].symbol == player && board[2][i].symbol == ' ') {
            return 2 * BOARD_SIZE + i;
        } else if (board[0][i].symbol == player && board[1][i].symbol == ' ' && board[2][i].symbol == player) {
            return 1 * BOARD_SIZE + i;
        } else if (board[0][i].symbol == ' ' && board[1][i].symbol == player && board[2][i].symbol == player) {
            return i;
        }
    }
    return -1;
}

// Fonction pour bloquer une colonne complétée par l'adversaire
int blockOpponentColumn(Cell board[BOARD_SIZE][BOARD_SIZE], char player) {
    char opponent = (player == 'X') ? 'O' : 'X';
    for (int i = 0; i < BOARD_SIZE; i++) {
        if (board[0][i].symbol == opponent && board[1][i].symbol == opponent && board[2][i].symbol == ' ') {
            return 2 * BOARD_SIZE + i;
        } else if (board[0][i].symbol == opponent && board[1][i].symbol == ' ' && board[2][i].symbol == opponent) {
            return 1 * BOARD_SIZE + i;
        } else if (board[0][i].symbol == ' ' && board[1][i].symbol == opponent && board[2][i].symbol == opponent) {
            return i;
        }
    }
    return -1;
}

// Fonction pour vérifier si une diagonale est complétée par le joueur
int checkDiagonalCompletion(Cell board[BOARD_SIZE][BOARD_SIZE], char player) {
    if (board[0][0].symbol == player && board[1][1].symbol == player && board[2][2].symbol == ' ') {
        return 8;
    } else if (board[0][0].symbol == player && board[1][1].symbol == ' ' && board[2][2].symbol == player) {
        return 4;
    } else if (board[0][0].symbol == ' ' && board[1][1].symbol == player && board[2][2].symbol == player) {
        return 0;
    } else if (board[0][2].symbol == player && board[1][1].symbol == player && board[2][0].symbol == ' ') {
        return 6;
    } else if (board[0][2].symbol == player && board[1][1].symbol == ' ' && board[2][0].symbol == player) {
        return 4;
    } else if (board[0][2].symbol == ' ' && board[1][1].symbol == player && board[2][0].symbol == player) {
        return 2;
    }
    return -1;
}

// Fonction pour bloquer une diagonale complétée par l'adversaire
int blockOpponentDiagonal(Cell board[BOARD_SIZE][BOARD_SIZE], char player) {
    char opponent = (player == 'X') ? 'O' : 'X';
    if (board[0][0].symbol == opponent && board[1][1].symbol == opponent && board[2][2].symbol == ' ') {
        return 8;
    } else if (board[0][0].symbol == opponent && board[1][1].symbol == ' ' && board[2][2].symbol == opponent) {
        return 4;
    } else if (board[0][0].symbol == ' ' && board[1][1].symbol == opponent && board[2][2].symbol == opponent) {
        return 0;
    } else if (board[0][2].symbol == opponent && board[1][1].symbol == opponent && board[2][0].symbol == ' ') {
        return 6;
    } else if (board[0][2].symbol == opponent && board[1][1].symbol == ' ' && board[2][0].symbol == opponent) {
        return 4;
    } else if (board[0][2].symbol == ' ' && board[1][1].symbol == opponent && board[2][0].symbol == opponent) {
        return 2;
    }
    return -1;
}

// Fonction pour vérifier s'il y a une possibilité de victoire pour le joueur
int checkPotentialWin(Cell board[BOARD_SIZE][BOARD_SIZE], char player) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        if (board[i][0].symbol == player && board[i][1].symbol == player && board[i][2].symbol == ' ') {
            return i * BOARD_SIZE + 2;
        } else if (board[i][0].symbol == player && board[i][1].symbol == ' ' && board[i][2].symbol == player) {
            return i * BOARD_SIZE + 1;
        } else if (board[i][0].symbol == ' ' && board[i][1].symbol == player && board[i][2].symbol == player) {
            return i * BOARD_SIZE;
        }
    }

    for (int i = 0; i < BOARD_SIZE; i++) {
        if (board[0][i].symbol == player && board[1][i].symbol == player && board[2][i].symbol == ' ') {
            return 2 * BOARD_SIZE + i;
        } else if (board[0][i].symbol == player && board[1][i].symbol == ' ' && board[2][i].symbol == player) {
            return 1 * BOARD_SIZE + i;
        } else if (board[0][i].symbol == ' ' && board[1][i].symbol == player && board[2][i].symbol == player) {
            return i;
        }
    }

    if (board[0][0].symbol == player && board[1][1].symbol == player && board[2][2].symbol == ' ') {
        return 8;
    } else if (board[0][0].symbol == player && board[1][1].symbol == ' ' && board[2][2].symbol == player) {
        return 4;
    } else if (board[0][0].symbol == ' ' && board[1][1].symbol == player && board[2][2].symbol == player) {
        return 0;
    } else if (board[0][2].symbol == player && board[1][1].symbol == player && board[2][0].symbol == ' ') {
        return 6;
    } else if (board[0][2].symbol == player && board[1][1].symbol == ' ' && board[2][0].symbol == player) {
        return 4;
    } else if (board[0][2].symbol == ' ' && board[1][1].symbol == player && board[2][0].symbol == player) {
        return 2;
    }

    return -1;
}

// Fonction pour bloquer une possibilité de victoire de l'adversaire
int blockPotentialWin(Cell board[BOARD_SIZE][BOARD_SIZE], char player) {
    char opponent = (player == 'X') ? 'O' : 'X';
    for (int i = 0; i < BOARD_SIZE; i++) {
        if (board[i][0].symbol == opponent && board[i][1].symbol == opponent && board[i][2].symbol == ' ') {
            return i * BOARD_SIZE + 2;
        } else if (board[i][0].symbol == opponent && board[i][1].symbol == ' ' && board[i][2].symbol == opponent) {
            return i * BOARD_SIZE + 1;
        } else if (board[i][0].symbol == ' ' && board[i][1].symbol == opponent && board[i][2].symbol == opponent) {
            return i * BOARD_SIZE;
        }
    }

    for (int i = 0; i < BOARD_SIZE; i++) {
        if (board[0][i].symbol == opponent && board[1][i].symbol == opponent && board[2][i].symbol == ' ') {
            return 2 * BOARD_SIZE + i;
        } else if (board[0][i].symbol == opponent && board[1][i].symbol == ' ' && board[2][i].symbol == opponent) {
            return 1 * BOARD_SIZE + i;
        } else if (board[0][i].symbol == ' ' && board[1][i].symbol == opponent && board[2][i].symbol == opponent) {
            return i;
        }
    }

    if (board[0][0].symbol == opponent && board[1][1].symbol == opponent && board[2][2].symbol == ' ') {
        return 8;
    } else if (board[0][0].symbol == opponent && board[1][1].symbol == ' ' && board[2][2].symbol == opponent) {
        return 4;
    } else if (board[0][0].symbol == ' ' && board[1][1].symbol == opponent && board[2][2].symbol == opponent) {
        return 0;
    } else if (board[0][2].symbol == opponent && board[1][1].symbol == opponent && board[2][0].symbol == ' ') {
        return 6;
    } else if (board[0][2].symbol == opponent && board[1][1].symbol == ' ' && board[2][0].symbol == opponent) {
        return 4;
    } else if (board[0][2].symbol == ' ' && board[1][1].symbol == opponent && board[2][0].symbol == opponent) {
        return 2;
    }

    return -1;
}
/**/


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define BOARD_SIZE 3
#define NUM_DISCRETE_STATES 1000

typedef struct {
    char symbol;
    bool occupied;
} Cell;

// Déclaration des règles
int checkRowCompletion(Cell board[BOARD_SIZE][BOARD_SIZE], char player);
int blockOpponentRow(Cell board[BOARD_SIZE][BOARD_SIZE], char player);
// Ajoutez d'autres règles ici...

// Structure pour représenter une règle
typedef struct {
    int (*ruleFunction)(Cell[BOARD_SIZE][BOARD_SIZE], char); // Pointeur vers la fonction de la règle
    int weight; // Poids de la règle pour l'évaluation
} Rule;

// Tableau des règles
Rule rules[] = {
    {checkRowCompletion, 3},
    {blockOpponentRow, 2},
    // Ajoutez d'autres règles ici...
};

// Fonction pour choisir la meilleure action (numéro de case) en fonction des valeurs Q et des règles
int chooseAction(Cell board[BOARD_SIZE][BOARD_SIZE], char player, double Q[BOARD_SIZE * BOARD_SIZE][NUM_DISCRETE_STATES]) {
    int action = -1;
    int bestAction = -1;
    int bestActionValue = -1;

    // Itérer sur toutes les règles
    for (int i = 0; i < sizeof(rules) / sizeof(rules[0]); i++) {
        if (rules[i].ruleFunction(board, player) >= 0) {
            // Règle applicable, évaluer l'action associée à cette règle
            action = rules[i].ruleFunction(board, player);

            // Évaluer l'action en utilisant les valeurs Q (ici, nous utilisons une valeur aléatoire pour l'exemple)
            int actionValue = rand() % 1000;

            // Mettre à jour la meilleure action si l'évaluation de cette action est meilleure que la meilleure action actuelle
            if (actionValue > bestActionValue) {
                bestAction = action;
                bestActionValue = actionValue;
            }
        }
    }

    // Si aucune règle n'est applicable ou si aucune règle n'a donné une action meilleure que les autres, choisir une action aléatoire
    if (bestAction == -1) {
        int emptyCells[BOARD_SIZE * BOARD_SIZE];
        int emptyCellCount = 0;
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                if (!board[i][j].occupied) {
                    emptyCells[emptyCellCount] = i * BOARD_SIZE + j;
                    emptyCellCount++;
                }
            }
        }
        return emptyCells[rand() % emptyCellCount];
    }

    return bestAction;
}

// Fonction pour entraîner l'IA en mettant à jour les poids des règles en fonction du résultat du jeu
void trainIA(double Q[BOARD_SIZE * BOARD_SIZE][NUM_DISCRETE_STATES], Cell board[BOARD_SIZE][BOARD_SIZE], char currentPlayer) {
    char opponent = (currentPlayer == 'X') ? 'O' : 'X';
    int winner = checkWin(board, currentPlayer);

    // Mettre à jour les poids des règles en fonction du résultat du jeu
    for (int i = 0; i < sizeof(rules) / sizeof(rules[0]); i++) {
        int ruleAction = rules[i].ruleFunction(board, currentPlayer);
        if (ruleAction >= 0) {
            int indexState = discretizeState(board, currentPlayer);
            double reward = 0.0;

            if (winner == 1) {
                reward = 1.0; // Joueur actuel (IA) a gagné
            } else if (winner == -1) {
                reward = -1.0; // Joueur actuel (IA) a perdu
            } else if (isBoardFull(board)) {
                reward = 0.5; // Match nul
            } else {
                // Le jeu continue, on évalue l'action en fonction des valeurs Q
                int maxQValue = -1;
                for (int a = 0; a < BOARD_SIZE * BOARD_SIZE; a++) {
                    if (Q[ruleAction][a] > maxQValue) {
                        maxQValue = Q[ruleAction][a];
                    }
                }
                reward = maxQValue;
            }

            // Mise à jour du poids de la règle avec la récompense calculée
            rules[i].weight += reward;
        }
    }
}
 /**/


 // Fonction pour choisir la meilleure action (numéro de case) en fonction des règles et de leur priorité
int chooseAction(Cell board[BOARD_SIZE][BOARD_SIZE], char player, Rule rules[NUM_RULES]) {
    for (int i = 0; i < NUM_RULES; i++) {
        if (rules[i].ruleFunction(board, player) >= 0) {
            return rules[i].ruleFunction(board, player);
        }
    }

    // Si aucune règle n'est applicable, choisir une action aléatoire
    int emptyCells[BOARD_SIZE * BOARD_SIZE];
    int emptyCellCount = 0;
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            if (!board[i][j].occupied) {
                emptyCells[emptyCellCount] = i * BOARD_SIZE + j;
                emptyCellCount++;
            }
        }
    }
    return emptyCells[rand() % emptyCellCount];
}
  /**/


  #include <stdbool.h>

// Fonction pour permettre à l'IA de jouer contre un joueur humain
void playAgainstAI(double Q[BOARD_SIZE * BOARD_SIZE][NUM_DISCRETE_STATES], SDL_Renderer* renderer, TTF_Font* font) {
    Cell board[BOARD_SIZE][BOARD_SIZE];
    initializeBoard(board);

    char currentPlayer = 'X';
    bool gameWon = false;

    Rule rules[NUM_RULES];
    initializeRules(rules);

    printf("Bienvenue dans le jeu Tic-Tac-Toe contre l'IA entraînée !\n");
    printf("Vous jouez en tant que X. Cliquez sur une case pour placer votre symbole.\n");

    SDL_Event event; // Variable pour stocker les événements SDL

    while (!gameWon && !isBoardFull(board)) {
        // Affichage du plateau de jeu actuel
        displayBoardSDL(board, renderer, font);

        if (currentPlayer == 'X') {
            // Tour du joueur humain
            int move = -1;

            while (move == -1) {
                SDL_WaitEvent(&event); // Attendre un événement

                if (event.type == SDL_QUIT) {
                    // Arrêter le jeu si l'utilisateur ferme la fenêtre
                    return;
                }

                move = getUserMove(event);
            }

            // Vérification de la validité du mouvement
            if (!board[move / BOARD_SIZE][move % BOARD_SIZE].occupied) {
                placeSymbol(board, move / BOARD_SIZE, move % BOARD_SIZE, currentPlayer);

                if (checkWin(board, currentPlayer)) {
                    printf("Félicitations ! Vous avez gagné contre l'IA.\n");
                    gameWon = true;
                } else {
                    currentPlayer = 'O'; // Passage à l'IA pour son tour
                }
            }
        } else {
            // Tour de l'IA
            int action = chooseAction(board, currentPlayer, rules);

            int row = action / BOARD_SIZE;
            int col = action % BOARD_SIZE;

            placeSymbol(board, row, col, currentPlayer);

            if (checkWin(board, currentPlayer)) {
                printf("Dommage ! L'IA a gagné cette fois.\n");
                gameWon = true;
            } else {
                currentPlayer = 'X'; // Passage au joueur humain pour son tour
            }
        }

        displayBoardSDL(board, renderer, font);

        if (gameWon || isBoardFull(board)) {
            // Entraîner l'IA à partir du mouvement du joueur humain
            trainIA(Q, board, currentPlayer, rules);
            bool continueGame = showGameOverMessage(renderer, font);

            if (continueGame) {
                // Réinitialiser le plateau pour une nouvelle partie
                initializeBoard(board);
                currentPlayer = 'X';
                gameWon = false;
            } else {
                // Arrêter le jeu si le joueur ne souhaite pas continuer
                return;
            }
        }
    }

    // Affichage du plateau final
    displayBoardSDL(board, renderer, font);
}


#include <math.h>
#include <stdlib.h>

// Fonction d'évaluation d'une règle
int evaluateRule(Cell board[BOARD_SIZE][BOARD_SIZE], char player, Rule rule) {
    return rule.ruleFunction(board, player);
}

// Fonction pour entraîner l'IA en utilisant les règles et les actions du joueur humain
void trainIA(double Q[BOARD_SIZE * BOARD_SIZE][NUM_DISCRETE_STATES], Cell board[BOARD_SIZE][BOARD_SIZE], char player, Rule rules[NUM_RULES]) {
    double learningRate = 0.1;
    double discountFactor = 0.9;

    // Obtenir l'état canonique du plateau actuel pour l'IA
    int canonicalState = getCanonicalState(board, player);

    // Obtenir l'index de l'état discretisé dans le tableau Q
    int stateIndex = discretizeState(canonicalState);

    // Choisir la règle applicable avec la plus haute priorité
    int bestRuleIndex = -1;
    int bestRuleValue = -1;

    for (int i = 0; i < NUM_RULES; i++) {
        if (rules[i].ruleFunction(board, player) >= 0) {
            int ruleValue = evaluateRule(board, player, rules[i]);
            if (ruleValue > bestRuleValue) {
                bestRuleIndex = i;
                bestRuleValue = ruleValue;
            }
        }
    }

    if (bestRuleIndex >= 0) {
        // Si une règle applicable a été trouvée, mettre à jour les valeurs de Q en fonction de cette règle
        for (int action = 0; action < BOARD_SIZE * BOARD_SIZE; action++) {
            int newState = rules[bestRuleIndex].actionFunction(board, player, action);
            int newStateIndex = discretizeState(newState);

            // Mettre à jour la valeur Q en utilisant la règle et les taux d'apprentissage et de remise
            Q[action][stateIndex] += learningRate * (bestRuleValue + discountFactor * Q[action][newStateIndex] - Q[action][stateIndex]);
        }
    } else {
        // Si aucune règle applicable n'a été trouvée, effectuer une exploration aléatoire
        int emptyCells[BOARD_SIZE * BOARD_SIZE];
        int emptyCellCount = 0;
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                if (!board[i][j].occupied) {
                    emptyCells[emptyCellCount] = i * BOARD_SIZE + j;
                    emptyCellCount++;
                }
            }
        }

        if (emptyCellCount > 0) {
            int randomAction = emptyCells[rand() % emptyCellCount];
            int newState = randomAction; // L'état ne change pas car c'est une exploration aléatoire
            int newStateIndex = discretizeState(newState);

            // Mettre à jour la valeur Q en utilisant la récompense nulle pour une exploration aléatoire
            Q[randomAction][stateIndex] += learningRate * (0 + discountFactor * Q[randomAction][newStateIndex] - Q[randomAction][stateIndex]);
        }
    }
}

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

// Définition des constantes du jeu
#define BOARD_SIZE 3
#define NUM_RULES 4

// Structure pour représenter une case du plateau de jeu
typedef struct {
    char symbol;
    bool occupied;
} Cell;

// Structure pour représenter une règle
typedef struct {
    bool (*ruleFunction)(Cell board[BOARD_SIZE][BOARD_SIZE], char player);
    int priority;
    int (*actionFunction)(Cell board[BOARD_SIZE][BOARD_SIZE], char player);
} Rule;

// Déclaration des fonctions
void initializeBoard(Cell board[BOARD_SIZE][BOARD_SIZE]);
void displayBoardSDL(Cell board[BOARD_SIZE][BOARD_SIZE], SDL_Renderer* renderer, TTF_Font* font);
bool checkWin(Cell board[BOARD_SIZE][BOARD_SIZE], char player);
bool isBoardFull(Cell board[BOARD_SIZE][BOARD_SIZE]);
int getUserMove(SDL_Event event);
int chooseAction(Cell board[BOARD_SIZE][BOARD_SIZE], char player, Rule rules[NUM_RULES]);
void trainIA(Cell board[BOARD_SIZE][BOARD_SIZE], char player, Rule rules[NUM_RULES]);

// Définition des règles et de leur priorité
bool checkRowCompletion(Cell board[BOARD_SIZE][BOARD_SIZE], char player);
bool blockOpponentRow(Cell board[BOARD_SIZE][BOARD_SIZE], char player);
bool checkColumnCompletion(Cell board[BOARD_SIZE][BOARD_SIZE], char player);
bool blockOpponentColumn(Cell board[BOARD_SIZE][BOARD_SIZE], char player);

// Tableau de règles avec leur priorité
Rule rules[NUM_RULES] = {
    {checkRowCompletion, 3, chooseAction},
    {blockOpponentRow, 2, chooseAction},
    {checkColumnCompletion, 1, chooseAction},
    {blockOpponentColumn, 0, chooseAction}
};

// Fonction principale pour permettre au joueur de jouer contre l'IA
void playAgainstIA(SDL_Renderer* renderer, TTF_Font* font) {
    Cell board[BOARD_SIZE][BOARD_SIZE];
    initializeBoard(board);

    char currentPlayer = 'X';
    bool gameWon = false;

    printf("Bienvenue dans le jeu Tic-Tac-Toe contre l'IA entraînée !\n");
    printf("Vous jouez en tant que X. Cliquez sur une case pour placer votre symbole.\n");

    SDL_Event event; // Variable pour stocker les événements SDL

    while (!gameWon && !isBoardFull(board)) {
        // Affichage du plateau de jeu actuel
        displayBoardSDL(board, renderer, font);

        if (currentPlayer == 'X') {
            // Tour du joueur humain
            int move = -1;

            while (move == -1) {
                SDL_WaitEvent(&event); // Attendre un événement

                if (event.type == SDL_QUIT) {
                    // Arrêter le jeu si l'utilisateur ferme la fenêtre
                    return;
                }

                move = getUserMove(event);
            }

            // Vérification de la validité du mouvement
            if (!board[move / BOARD_SIZE][move % BOARD_SIZE].occupied) {
                placeSymbol(board, move / BOARD_SIZE, move % BOARD_SIZE, currentPlayer);

                if (checkWin(board, currentPlayer)) {
                    printf("Félicitations ! Vous avez gagné contre l'IA.\n");
                    gameWon = true;
                } else {
                    currentPlayer = 'O'; // Passage à l'IA pour son tour
                }
            }
        } else {
            // Tour de l'IA
            trainIA(board, currentPlayer, rules); // Entraîner l'IA à partir des actions du joueur humain

            int action = chooseAction(board, currentPlayer, rules); // Choisir une action avec le cerveau de règles
            int row = action / BOARD_SIZE;
            int col = action % BOARD_SIZE;

            placeSymbol(board, row, col, currentPlayer);

            if (checkWin(board, currentPlayer)) {
                gameWon = true;
                printf("Dommage ! L'IA a gagné cette fois.\n");
            } else {
                currentPlayer = 'X'; // Passage au joueur humain pour son tour
            }
        }
    }

    // Affichage du plateau final
    displayBoardSDL(board, renderer, font);
}

// Vous pouvez définir les règles et leur implémentation ici
// Par exemple, une règle pour vérifier si une rangée est complétée par un joueur
bool checkRowCompletion(Cell board[BOARD_SIZE][BOARD_SIZE], char player) {
    // Implémentation de la règle ici
}

// Une autre règle pour bloquer l'adversaire s'il complète une rangée
bool blockOpponentRow(Cell board[BOARD_SIZE][BOARD_SIZE], char player) {
    // Implémentation de la règle ici
}

// Vous pouvez définir d'autres règles et leurs implémentations ici

// La fonction principale du jeu
int main() {
    // Initialiser SDL et créer une fenêtre pour afficher le jeu
    // ...

    // Charger la police pour l'affichage du texte
    // ...

    // Jouer le jeu contre l'IA
    playAgainstIA(renderer, font);

    // Libérer les ressources SDL
    // ...

    return 0;
}



// Fonction pour entraîner l'IA en fonction des actions du joueur humain
void trainIA(Cell board[BOARD_SIZE][BOARD_SIZE], char player, Rule rules[NUM_RULES]) {
    // Vérifier si le joueur humain a gagné dans l'état actuel du plateau
    bool playerWon = checkWin(board, player);

    // Parcourir les règles et mettre à jour le cerveau de règles en fonction des actions du joueur
    for (int i = 0; i < NUM_RULES; i++) {
        // Vérifier si la règle est applicable dans l'état actuel du plateau
        if (rules[i].ruleFunction(board, player)) {
            int action = rules[i].actionFunction(board, player);

            // Mettre à jour le cerveau de règles avec la priorité de la règle si le joueur a gagné
            if (playerWon) {
                // Augmenter la priorité de la règle si le joueur a gagné
                rules[i].priority++;
            } else {
                // Diminuer la priorité de la règle si le joueur n'a pas gagné
                rules[i].priority--;
            }

            // Mettre à jour le cerveau de règles pour l'action choisie
            int state = discretizeState(board, player);
            Q[action][state] = rules[i].priority;
        }
    }
}
