# Définition des noms de fichiers sources
SRCS = main.c tictactoe.c qlearning.c

# Définition des noms des fichiers objets générés par la compilation
OBJS = $(SRCS:.c=.o)

# Définition du compilateur et des options de compilation
CC = gcc
CFLAGS = -Wall -Wextra -std=c99

# Nom de l'exécutable final
EXECUTABLE = tictactoe

# Règle de compilation pour l'exécutable
$(EXECUTABLE): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $(EXECUTABLE)

# Règle de compilation pour les fichiers objets
%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

# Règle pour nettoyer les fichiers objets et l'exécutable
clean:
	rm -f $(OBJS) $(EXECUTABLE)
