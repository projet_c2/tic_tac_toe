#include <stdio.h>
#include <stdbool.h>

#define BOARD_SIZE 3

// Structure pour représenter une case du plateau
typedef struct {
    char symbol; // Contient 'X', 'O' ou un caractère vide ('.')
    bool occupied; // Indique si la case est occupée ou non
} Cell;

// Fonction pour initialiser le plateau avec des cases vides
void initializeBoard(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            board[i][j].symbol = '.';
            board[i][j].occupied = false;
        }
    }
}

// Fonction pour afficher le plateau de jeu
void displayBoard(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            printf(" %c ", board[i][j].symbol);
            if (j < BOARD_SIZE - 1)
                printf("|");
        }
        printf("\n");
        if (i < BOARD_SIZE - 1)
            printf("-----------\n");
    }
}

// Fonction pour vérifier si le plateau est plein (match nul)
bool isBoardFull(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            if (!board[i][j].occupied)
                return false;
        }
    }
    return true;
}

// Fonction pour vérifier si un joueur a gagné la partie
bool checkWin(Cell board[BOARD_SIZE][BOARD_SIZE], char player) {
    // Vérification des lignes et des colonnes
    for (int i = 0; i < BOARD_SIZE; i++) {
        if ((board[i][0].symbol == player && board[i][1].symbol == player && board[i][2].symbol == player) ||
            (board[0][i].symbol == player && board[1][i].symbol == player && board[2][i].symbol == player)) {
            return true;
        }
    }
    // Vérification des diagonales
    if ((board[0][0].symbol == player && board[1][1].symbol == player && board[2][2].symbol == player) ||
        (board[0][2].symbol == player && board[1][1].symbol == player && board[2][0].symbol == player)) {
        return true;
    }
    return false;
}

// Fonction pour placer le symbole d'un joueur sur le plateau
void placeSymbol(Cell board[BOARD_SIZE][BOARD_SIZE], int row, int col, char player) {
    board[row][col].symbol = player;
    board[row][col].occupied = true;
}

int main() {
    Cell board[BOARD_SIZE][BOARD_SIZE];
    initializeBoard(board);

    char currentPlayer = 'X';
    int row, col;
    bool gameWon = false;

    printf("Bienvenue dans le jeu Tic-Tac-Toe !\n");

    while (!gameWon && !isBoardFull(board)) {
        displayBoard(board);
        printf("Joueur %c, c'est à votre tour. Entrez le numéro de ligne (0-2) : ", currentPlayer);
        scanf("%d", &row);
        printf("Entrez le numéro de colonne (0-2) : ");
        scanf("%d", &col);

        if (row >= 0 && row < BOARD_SIZE && col >= 0 && col < BOARD_SIZE && !board[row][col].occupied) {
            placeSymbol(board, row, col, currentPlayer);

            if (checkWin(board, currentPlayer)) {
                gameWon = true;
                printf("Félicitations, joueur %c a gagné !\n", currentPlayer);
            } else {
                currentPlayer = (currentPlayer == 'X') ? 'O' : 'X'; // Changer de joueur
            }
        } else {
            printf("Mouvement invalide. Réessayez.\n");
        }
    }

    if (!gameWon) {
        printf("Partie terminée. Match nul !\n");
    }

    displayBoard(board);

    return 0;
}



/////////////////////////////////////////////


#include <stdio.h>
#include <stdbool.h>

#define BOARD_SIZE 3
#define MAX_EPISODES 10000
#define LEARNING_RATE 0.1
#define DISCOUNT_FACTOR 0.9
#define EXPLORATION_PROB 0.3

// Structure pour représenter une case du plateau
typedef struct {
    char symbol; // Contient 'X', 'O' ou un caractère vide ('.')
    bool occupied; // Indique si la case est occupée ou non
} Cell;

// Fonction pour initialiser le plateau avec des cases vides
void initializeBoard(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    // ...
}

// Fonction pour afficher le plateau de jeu
void displayBoard(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    // ...
}

// Fonction pour vérifier si le plateau est plein (match nul)
bool isBoardFull(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    // ...
}

// Fonction pour vérifier si un joueur a gagné la partie
bool checkWin(Cell board[BOARD_SIZE][BOARD_SIZE], char player) {
    // ...
}

// Fonction pour placer le symbole d'un joueur sur le plateau
void placeSymbol(Cell board[BOARD_SIZE][BOARD_SIZE], int row, int col, char player) {
    // ...
}

// Fonction pour choisir la meilleure action (numéro de case) en fonction des valeurs Q
int chooseAction(Cell board[BOARD_SIZE][BOARD_SIZE], char player, double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE]) {
    // Exploration aléatoire avec une certaine probabilité
    if ((double)rand() / RAND_MAX < EXPLORATION_PROB) {
        int emptyCells[BOARD_SIZE * BOARD_SIZE];
        int emptyCellCount = 0;
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                if (!board[i][j].occupied) {
                    emptyCells[emptyCellCount] = i * BOARD_SIZE + j;
                    emptyCellCount++;
                }
            }
        }
        return emptyCells[rand() % emptyCellCount];
    } else {
        // Exploitation : choisir la meilleure action selon les valeurs Q
        int maxAction = -1;
        double maxQ = -1;
        for (int i = 0; i < BOARD_SIZE * BOARD_SIZE; i++) {
            if (!board[i / BOARD_SIZE][i % BOARD_SIZE].occupied) {
                if (Q[i][player - 'X'] > maxQ) {
                    maxQ = Q[i][player - 'X'];
                    maxAction = i;
                }
            }
        }
        return maxAction;
    }
}

// Fonction pour entraîner l'IA en utilisant l'algorithme Q-learning
void trainAI() {
    Cell board[BOARD_SIZE][BOARD_SIZE];
    initializeBoard(board);

    double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE] = {0}; // Tableau des valeurs Q

    for (int episode = 0; episode < MAX_EPISODES; episode++) {
        // Initialisation du jeu
        initializeBoard(board);
        char currentPlayer = 'X';
        bool gameWon = false;

        while (!gameWon && !isBoardFull(board)) {
            int action = chooseAction(board, currentPlayer, Q);

            int row = action / BOARD_SIZE;
            int col = action % BOARD_SIZE;

            placeSymbol(board, row, col, currentPlayer);

            if (checkWin(board, currentPlayer)) {
                gameWon = true;
                // Récompense +1 pour la victoire
                Q[action][currentPlayer - 'X'] += LEARNING_RATE * (1 - Q[action][currentPlayer - 'X']);
            } else if (isBoardFull(board)) {
                // Récompense 0 pour un match nul
                Q[action][currentPlayer - 'X'] += LEARNING_RATE * (0 - Q[action][currentPlayer - 'X']);
            } else {
                // Le jeu continue
                char nextPlayer = (currentPlayer == 'X') ? 'O' : 'X';
                int nextAction = chooseAction(board, nextPlayer, Q);
                // Mise à jour de la valeur Q avec l'algorithme Q-learning
                Q[action][currentPlayer - 'X'] += LEARNING_RATE * (DISCOUNT_FACTOR * Q[nextAction][nextPlayer - 'X'] - Q[action][currentPlayer - 'X']);
            }

            currentPlayer = (currentPlayer == 'X') ? 'O' : 'X'; // Changer de joueur
        }
    }

    // Après l'entraînement, vous pouvez enregistrer le tableau de valeurs Q pour une utilisation ultérieure
    // (par exemple, pour jouer contre l'IA entraînée).
}

// Fonction pour permettre à l'IA de jouer contre un joueur humain
void playAgainstAI() {
    // Initialiser le plateau et le tableau de valeurs Q comme précédemment
    // ...

    // Mettre en place la logique de jeu où l'IA joue contre le joueur humain en utilisant Q pour prendre des décisions.
    // ...
}

int main() {
    // Appel de la fonction pour entraîner l'IA
    trainAI();

    // Appel de la fonction pour jouer contre l'IA entraînée
    playAgainstAI();

    return 0;
}



///////////////////////////////////////////////////////////




#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h> // Pour la fonction rand()

#define BOARD_SIZE 3
#define MAX_EPISODES 10000
#define LEARNING_RATE 0.1
#define DISCOUNT_FACTOR 0.9
#define EXPLORATION_PROB 0.3

// Structure pour représenter une case du plateau
typedef struct {
    char symbol; // Contient 'X', 'O' ou un caractère vide ('.')
    bool occupied; // Indique si la case est occupée ou non
} Cell;

// ... (Implémentation des autres fonctions comme dans la précédente réponse)

// Fonction pour permettre à l'IA de jouer contre un joueur humain
void playAgainstAI(double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE]) {
    Cell board[BOARD_SIZE][BOARD_SIZE];
    initializeBoard(board);

    char currentPlayer = 'X';
    bool gameWon = false;

    printf("Bienvenue dans le jeu Tic-Tac-Toe contre l'IA entraînée !\n");
    printf("Vous jouez en tant que X. Entrez le numéro de ligne (0-2) et le numéro de colonne (0-2) pour placer votre symbole.\n");

    while (!gameWon && !isBoardFull(board)) {
        // Affichage du plateau de jeu actuel
        displayBoard(board);

        if (currentPlayer == 'X') {
            // Tour du joueur humain
            int row, col;
            printf("C'est à votre tour. Entrez le numéro de ligne : ");
            scanf("%d", &row);
            printf("Entrez le numéro de colonne : ");
            scanf("%d", &col);

            // Vérification de la validité du mouvement
            if (row >= 0 && row < BOARD_SIZE && col >= 0 && col < BOARD_SIZE && !board[row][col].occupied) {
                placeSymbol(board, row, col, currentPlayer);

                if (checkWin(board, currentPlayer)) {
                    gameWon = true;
                    printf("Félicitations ! Vous avez gagné contre l'IA.\n");
                } else {
                    currentPlayer = 'O'; // Passage à l'IA pour son tour
                }
            } else {
                printf("Mouvement invalide. Réessayez.\n");
            }
        } else {
            // Tour de l'IA
            int action = chooseAction(board, currentPlayer, Q);

            int row = action / BOARD_SIZE;
            int col = action % BOARD_SIZE;

            printf("L'IA joue : ligne %d, colonne %d\n", row, col);
            placeSymbol(board, row, col, currentPlayer);

            if (checkWin(board, currentPlayer)) {
                gameWon = true;
                printf("Dommage ! L'IA a gagné cette fois.\n");
            } else {
                currentPlayer = 'X'; // Passage au joueur humain pour son tour
            }
        }
    }

    // Affichage du plateau final
    displayBoard(board);

    if (!gameWon) {
        printf("Partie terminée. Match nul !\n");
    }
}

int main() {
    // Appel de la fonction pour entraîner l'IA
    double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE] = {0};
    trainAI(Q);

    // Appel de la fonction pour jouer contre l'IA entraînée
    playAgainstAI(Q);

    return 0;
}




////////////////////////////////////


// Fonction de hachage djb2
unsigned long hash(const char *str) {
    unsigned long hash = 5381;
    int c;
    while ((c = *str++))
        hash = ((hash << 5) + hash) + c; // hash * 33 + c
    return hash;
}

// Fonction de discrétisation de l'état
unsigned int discretizeState(const char *state) {
    // Ici, nous utilisons la fonction de hachage djb2 pour obtenir l'indice discret
    unsigned long hashValue = hash(state);
    unsigned int discreteState = hashValue % NUM_DISCRETE_STATES; // NUM_DISCRETE_STATES est la taille du tableau Q discret
    return discreteState;
}

///

unsigned long hash(const char *str) {
    unsigned long hash = 5381;
    int c;
    while ((c = *str++))
        hash = ((hash << 5) + hash) + c; // hash * 33 + c
    return hash;
}

unsigned int discretizeState(const char *state) {
    unsigned long hashValue = hash(state);
    unsigned int discreteState = hashValue % NUM_DISCRETE_STATES;
    return discreteState;
}
////
unsigned long hash(const char *str) {
    unsigned long hash = 5381;
    int c;
    while ((c = *str++))
        hash = ((hash << 5) + hash) + c; // hash * 33 + c
    return hash;
}
////
unsigned int discretizeState(const char *state) {
    // Convertir l'état du plateau en une représentation canonique
    char canonicalState[10];
    getCanonicalState(state, canonicalState);

    // Discrétiser l'état du plateau
    unsigned long hashValue = hash(canonicalState);
    unsigned int discreteState = hashValue % NUM_DISCRETE_STATES;
    return discreteState;
}
/////
void getCanonicalState(const char *state, char *canonicalState) {
    // Ici, nous utilisons une représentation canonique simple en fixant le symbole "X" pour le joueur humain et le symbole "O" pour l'IA.
    // Vous pouvez ajuster cette fonction en fonction de votre choix de symboles pour le joueur humain et l'IA.

    for (int i = 0; i < 9; i++) {
        if (state[i] == 'X' || state[i] == 'O') {
            canonicalState[i] = state[i];
        } else {
            canonicalState[i] = '.'; // Marquer les cases vides avec "."
        }
    }
    canonicalState[9] = '\0'; // Terminer la chaîne de caractères avec le caractère nul.
}
////


//////////////////////////////////////////


from itertools import permutations

def apply_rotations_and_symmetries(state):
    rotations = [state]
    for _ in range(3):
        # Rotation de 90 degrés
        state = list(zip(*state[::-1]))
        rotations.append([''.join(row) for row in state])

    # Symétrie horizontale
    sym_horizontal = [row[::-1] for row in rotations[0:4]]
    rotations.extend(sym_horizontal)

    # Symétrie verticale
    sym_vertical = [row[::-1] for row in rotations[4:]]
    rotations.extend(sym_vertical)

    return rotations

def count_equivalent_states():
    all_states = set()
    empty_board = "........."

    # Enumérer toutes les combinaisons possibles du plateau de jeu
    for perm in permutations("XO.", 9):
        state = ''.join(perm)

        # Appliquer les rotations et les symétries pour obtenir toutes les configurations équivalentes
        equivalent_states = apply_rotations_and_symmetries(list(state))
        for equiv_state in equivalent_states:
            all_states.add(''.join(equiv_state))

    return len(all_states)

if __name__ == "__main__":
    num_equivalent_states = count_equivalent_states()
    print("Nombre de classes d'équivalence possibles:", num_equivalent_states)
///////////////////////////////////////////////////

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

// Définir NUM_DISCRETE_STATES avec la valeur choisie
#define NUM_DISCRETE_STATES 138 // Utilisez la valeur que vous avez obtenue pour le nombre de classes d'équivalence possibles.

// Structure pour représenter une action (mouvement) du joueur
typedef struct {
    int row;
    int col;
} Move;

// Fonction de hachage djb2
unsigned long hash(const char *str) {
    unsigned long hash = 5381;
    int c;
    while ((c = *str++))
        hash = ((hash << 5) + hash) + c; // hash * 33 + c
    return hash;
}

// Fonction de discrétisation de l'état
unsigned int discretizeState(const char *state) {
    // Convertir l'état du plateau en une représentation canonique
    char canonicalState[10];
    getCanonicalState(state, canonicalState);

    // Discrétiser l'état du plateau
    unsigned long hashValue = hash(canonicalState);
    unsigned int discreteState = hashValue % NUM_DISCRETE_STATES;
    return discreteState;
}

// ... (Le reste du code du jeu Tic-Tac-Toe)

int main() {
    // ... (Initialisation du jeu Tic-Tac-Toe)

    // Boucle principale du jeu
    while (!isGameOver(board)) {
        // ... (Tour du joueur humain et de l'IA)
        // Appeler la fonction de discrétisation pour obtenir l'indice discret
        unsigned int discreteState = discretizeState(board);

        // Maintenant, vous pouvez utiliser discreteState pour accéder à la valeur Q appropriée dans le tableau Q discret.
        // ... (Mise à jour du tableau Q discret et apprentissage par renforcement)

        // ... (Affichage du plateau, vérification de la fin du jeu, etc.)
    }

    // ... (Affichage du résultat final du jeu)

    return 0;
}
/////////////////////////////////////////////////////


/*  code de moi*/

#include <stdio.h>
#include <stdbool.h>

#define BOARD_SIZE 3

// Structure pour représenter une case du plateau
typedef struct {
    char symbol; // Contient 'X', 'O' ou un caractère vide ('.')
    bool occupied; // Indique si la case est occupée ou non
} Cell;

// Fonction pour initialiser le plateau avec des cases vides
void initializeBoard(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            board[i][j].symbol = '.';
            board[i][j].occupied = false;
        }
    }
}

// Fonction pour afficher le plateau de jeu
void displayBoard(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            printf(" %c ", board[i][j].symbol);
            if (j < BOARD_SIZE - 1)
                printf("|");
        }
        printf("\n");
        if (i < BOARD_SIZE - 1)
            printf("-----------\n");
    }
}

// Fonction pour vérifier si le plateau est plein (match nul)
bool isBoardFull(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            if (!board[i][j].occupied)
                return false;
        }
    }
    return true;
}

// Fonction pour vérifier si un joueur a gagné la partie
bool checkWin(Cell board[BOARD_SIZE][BOARD_SIZE], char player) {
    // Vérification des lignes et des colonnes
    for (int i = 0; i < BOARD_SIZE; i++) {
        if ((board[i][0].symbol == player && board[i][1].symbol == player && board[i][2].symbol == player) ||
            (board[0][i].symbol == player && board[1][i].symbol == player && board[2][i].symbol == player)) {
            return true;
        }
    }
    // Vérification des diagonales
    if ((board[0][0].symbol == player && board[1][1].symbol == player && board[2][2].symbol == player) ||
        (board[0][2].symbol == player && board[1][1].symbol == player && board[2][0].symbol == player)) {
        return true;
    }
    return false;
}

// Fonction pour placer le symbole d'un joueur sur le plateau
void placeSymbol(Cell board[BOARD_SIZE][BOARD_SIZE], int row, int col, char player) {
    board[row][col].symbol = player;
    board[row][col].occupied = true;
}

int main() {
    Cell board[BOARD_SIZE][BOARD_SIZE];
    initializeBoard(board);

    char currentPlayer = 'X';
    int row, col;
    bool gameWon = false;

    printf("Bienvenue dans le jeu Tic-Tac-Toe !\n");

    while (!gameWon && !isBoardFull(board)) {
        displayBoard(board);
        printf("Joueur %c, c'est à votre tour. Entrez le numéro de ligne (0-2) : ", currentPlayer);
        scanf("%d", &row);
        printf("Entrez le numéro de colonne (0-2) : ");
        scanf("%d", &col);

        if (row >= 0 && row < BOARD_SIZE && col >= 0 && col < BOARD_SIZE && !board[row][col].occupied) {
            placeSymbol(board, row, col, currentPlayer);

            if (checkWin(board, currentPlayer)) {
                gameWon = true;
                printf("Félicitations, joueur %c a gagné !\n", currentPlayer);
            } else {
                currentPlayer = (currentPlayer == 'X') ? 'O' : 'X'; // Changer de joueur
            }
        } else {
            printf("Mouvement invalide. Réessayez.\n");
        }
    }

    if (!gameWon) {
        printf("Partie terminée. Match nul !\n");
    }

    displayBoard(board);

    return 0;
}
"
et suite :
"#include <stdio.h>
#include <stdbool.h>

#define BOARD_SIZE 3
#define MAX_EPISODES 10000
#define LEARNING_RATE 0.1
#define DISCOUNT_FACTOR 0.9
#define EXPLORATION_PROB 0.3

// Structure pour représenter une case du plateau
typedef struct {
    char symbol; // Contient 'X', 'O' ou un caractère vide ('.')
    bool occupied; // Indique si la case est occupée ou non
} Cell;

// Fonction pour initialiser le plateau avec des cases vides
void initializeBoard(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    // ...
}

// Fonction pour afficher le plateau de jeu
void displayBoard(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    // ...
}

// Fonction pour vérifier si le plateau est plein (match nul)
bool isBoardFull(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    // ...
}

// Fonction pour vérifier si un joueur a gagné la partie
bool checkWin(Cell board[BOARD_SIZE][BOARD_SIZE], char player) {
    // ...
}

// Fonction pour placer le symbole d'un joueur sur le plateau
void placeSymbol(Cell board[BOARD_SIZE][BOARD_SIZE], int row, int col, char player) {
    // ...
}

// Fonction pour choisir la meilleure action (numéro de case) en fonction des valeurs Q
int chooseAction(Cell board[BOARD_SIZE][BOARD_SIZE], char player, double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE]) {
    // Exploration aléatoire avec une certaine probabilité
    if ((double)rand() / RAND_MAX < EXPLORATION_PROB) {
        int emptyCells[BOARD_SIZE * BOARD_SIZE];
        int emptyCellCount = 0;
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                if (!board[i][j].occupied) {
                    emptyCells[emptyCellCount] = i * BOARD_SIZE + j;
                    emptyCellCount++;
                }
            }
        }
        return emptyCells[rand() % emptyCellCount];
    } else {
        // Exploitation : choisir la meilleure action selon les valeurs Q
        int maxAction = -1;
        double maxQ = -1;
        for (int i = 0; i < BOARD_SIZE * BOARD_SIZE; i++) {
            if (!board[i / BOARD_SIZE][i % BOARD_SIZE].occupied) {
                if (Q[i][player - 'X'] > maxQ) {
                    maxQ = Q[i][player - 'X'];
                    maxAction = i;
                }
            }
        }
        return maxAction;
    }
}

// Fonction pour entraîner l'IA en utilisant l'algorithme Q-learning
void trainAI() {
    Cell board[BOARD_SIZE][BOARD_SIZE];
    initializeBoard(board);

    double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE] = {0}; // Tableau des valeurs Q

    for (int episode = 0; episode < MAX_EPISODES; episode++) {
        // Initialisation du jeu
        initializeBoard(board);
        char currentPlayer = 'X';
        bool gameWon = false;

        while (!gameWon && !isBoardFull(board)) {
            int action = chooseAction(board, currentPlayer, Q);

            int row = action / BOARD_SIZE;
            int col = action % BOARD_SIZE;

            placeSymbol(board, row, col, currentPlayer);

            if (checkWin(board, currentPlayer)) {
                gameWon = true;
                // Récompense +1 pour la victoire
                Q[action][currentPlayer - 'X'] += LEARNING_RATE * (1 - Q[action][currentPlayer - 'X']);
            } else if (isBoardFull(board)) {
                // Récompense 0 pour un match nul
                Q[action][currentPlayer - 'X'] += LEARNING_RATE * (0 - Q[action][currentPlayer - 'X']);
            } else {
                // Le jeu continue
                char nextPlayer = (currentPlayer == 'X') ? 'O' : 'X';
                int nextAction = chooseAction(board, nextPlayer, Q);
                // Mise à jour de la valeur Q avec l'algorithme Q-learning
                Q[action][currentPlayer - 'X'] += LEARNING_RATE * (DISCOUNT_FACTOR * Q[nextAction][nextPlayer - 'X'] - Q[action][currentPlayer - 'X']);
            }

            currentPlayer = (currentPlayer == 'X') ? 'O' : 'X'; // Changer de joueur
        }
    }

    // Après l'entraînement, vous pouvez enregistrer le tableau de valeurs Q pour une utilisation ultérieure
    // (par exemple, pour jouer contre l'IA entraînée).
}

// Fonction pour permettre à l'IA de jouer contre un joueur humain
void playAgainstAI() {
    // Initialiser le plateau et le tableau de valeurs Q comme précédemment
    // ...

    // Mettre en place la logique de jeu où l'IA joue contre le joueur humain en utilisant Q pour prendre des décisions.
    // ...
}

int main() {
    // Appel de la fonction pour entraîner l'IA
    trainAI();

    // Appel de la fonction pour jouer contre l'IA entraînée
    playAgainstAI();

    return 0;
}
"
et suite :
"#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h> // Pour la fonction rand()

#define BOARD_SIZE 3
#define MAX_EPISODES 10000
#define LEARNING_RATE 0.1
#define DISCOUNT_FACTOR 0.9
#define EXPLORATION_PROB 0.3

// Structure pour représenter une case du plateau
typedef struct {
    char symbol; // Contient 'X', 'O' ou un caractère vide ('.')
    bool occupied; // Indique si la case est occupée ou non
} Cell;

// ... (Implémentation des autres fonctions comme dans la précédente réponse)

// Fonction pour permettre à l'IA de jouer contre un joueur humain
void playAgainstAI(double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE]) {
    Cell board[BOARD_SIZE][BOARD_SIZE];
    initializeBoard(board);

    char currentPlayer = 'X';
    bool gameWon = false;

    printf("Bienvenue dans le jeu Tic-Tac-Toe contre l'IA entraînée !\n");
    printf("Vous jouez en tant que X. Entrez le numéro de ligne (0-2) et le numéro de colonne (0-2) pour placer votre symbole.\n");

    while (!gameWon && !isBoardFull(board)) {
        // Affichage du plateau de jeu actuel
        displayBoard(board);

        if (currentPlayer == 'X') {
            // Tour du joueur humain
            int row, col;
            printf("C'est à votre tour. Entrez le numéro de ligne : ");
            scanf("%d", &row);
            printf("Entrez le numéro de colonne : ");
            scanf("%d", &col);

            // Vérification de la validité du mouvement
            if (row >= 0 && row < BOARD_SIZE && col >= 0 && col < BOARD_SIZE && !board[row][col].occupied) {
                placeSymbol(board, row, col, currentPlayer);

                if (checkWin(board, currentPlayer)) {
                    gameWon = true;
                    printf("Félicitations ! Vous avez gagné contre l'IA.\n");
                } else {
                    currentPlayer = 'O'; // Passage à l'IA pour son tour
                }
            } else {
                printf("Mouvement invalide. Réessayez.\n");
            }
        } else {
            // Tour de l'IA
            int action = chooseAction(board, currentPlayer, Q);

            int row = action / BOARD_SIZE;
            int col = action % BOARD_SIZE;

            printf("L'IA joue : ligne %d, colonne %d\n", row, col);
            placeSymbol(board, row, col, currentPlayer);

            if (checkWin(board, currentPlayer)) {
                gameWon = true;
                printf("Dommage ! L'IA a gagné cette fois.\n");
            } else {
                currentPlayer = 'X'; // Passage au joueur humain pour son tour
            }
        }
    }

    // Affichage du plateau final
    displayBoard(board);

    if (!gameWon) {
        printf("Partie terminée. Match nul !\n");
    }
}

int main() {
    // Appel de la fonction pour entraîner l'IA
    double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE] = {0};
    trainAI(Q);

    // Appel de la fonction pour jouer contre l'IA entraînée
    playAgainstAI(Q);

    return 0;
}

/* fin de code de mùoi*/



/* contenu complet de ticta.c*/
#include "tictactoe.h"
#include <stdio.h>

// Fonction pour initialiser le plateau avec des cases vides
void initializeBoard(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            board[i][j].symbol = '.';
            board[i][j].occupied = false;
        }
    }
}

// Fonction pour afficher le plateau de jeu
void displayBoard(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            printf(" %c ", board[i][j].symbol);
            if (j < BOARD_SIZE - 1)
                printf("|");
        }
        printf("\n");
        if (i < BOARD_SIZE - 1)
            printf("-----------\n");
    }
}

// Fonction pour vérifier si le plateau est plein (match nul)
bool isBoardFull(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            if (!board[i][j].occupied)
                return false;
        }
    }
    return true;
}

// Fonction pour vérifier si un joueur a gagné la partie
bool checkWin(Cell board[BOARD_SIZE][BOARD_SIZE], char player) {
    // Vérification des lignes et des colonnes
    for (int i = 0; i < BOARD_SIZE; i++) {
        if ((board[i][0].symbol == player && board[i][1].symbol == player && board[i][2].symbol == player) ||
            (board[0][i].symbol == player && board[1][i].symbol == player && board[2][i].symbol == player)) {
            return true;
        }
    }
    // Vérification des diagonales
    if ((board[0][0].symbol == player && board[1][1].symbol == player && board[2][2].symbol == player) ||
        (board[0][2].symbol == player && board[1][1].symbol == player && board[2][0].symbol == player)) {
        return true;
    }
    return false;
}

// Fonction pour placer le symbole d'un joueur sur le plateau
void placeSymbol(Cell board[BOARD_SIZE][BOARD_SIZE], int row, int col, char player) {
    board[row][col].symbol = player;
    board[row][col].occupied = true;
}
 /*   fin de ticta.c*/



 
