#include <stdio.h>
#include <stdbool.h>

#define BOARD_SIZE 3

// Structure pour représenter une case du plateau
typedef struct {
    char symbol; // Contient 'X', 'O' ou un caractère vide ('.')
    bool occupied; // Indique si la case est occupée ou non
} Cell;

// Fonction pour initialiser le plateau avec des cases vides
void initializeBoard(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            board[i][j].symbol = '.';
            board[i][j].occupied = false;
        }
    }
}

// Fonction pour afficher le plateau de jeu
void displayBoard(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            printf(" %c ", board[i][j].symbol);
            if (j < BOARD_SIZE - 1)
                printf("|");
        }
        printf("\n");
        if (i < BOARD_SIZE - 1)
            printf("-----------\n");
    }
}

// Fonction pour vérifier si le plateau est plein (match nul)
bool isBoardFull(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            if (!board[i][j].occupied)
                return false;
        }
    }
    return true;
}

// Fonction pour vérifier si un joueur a gagné la partie
bool checkWin(Cell board[BOARD_SIZE][BOARD_SIZE], char player) {
    // Vérification des lignes et des colonnes
    for (int i = 0; i < BOARD_SIZE; i++) {
        if ((board[i][0].symbol == player && board[i][1].symbol == player && board[i][2].symbol == player) ||
            (board[0][i].symbol == player && board[1][i].symbol == player && board[2][i].symbol == player)) {
            return true;
        }
    }
    // Vérification des diagonales
    if ((board[0][0].symbol == player && board[1][1].symbol == player && board[2][2].symbol == player) ||
        (board[0][2].symbol == player && board[1][1].symbol == player && board[2][0].symbol == player)) {
        return true;
    }
    return false;
}

// Fonction pour placer le symbole d'un joueur sur le plateau
void placeSymbol(Cell board[BOARD_SIZE][BOARD_SIZE], int row, int col, char player) {
    board[row][col].symbol = player;
    board[row][col].occupied = true;
}

int main() {
    Cell board[BOARD_SIZE][BOARD_SIZE];
    initializeBoard(board);

    char currentPlayer = 'X';
    int row, col;
    bool gameWon = false;

    printf("Bienvenue dans le jeu Tic-Tac-Toe !\n");

    while (!gameWon && !isBoardFull(board)) {
        displayBoard(board);
        printf("Joueur %c, c'est à votre tour. Entrez le numéro de ligne (0-2) : ", currentPlayer);
        scanf("%d", &row);
        printf("Entrez le numéro de colonne (0-2) : ");
        scanf("%d", &col);

        if (row >= 0 && row < BOARD_SIZE && col >= 0 && col < BOARD_SIZE && !board[row][col].occupied) {
            placeSymbol(board, row, col, currentPlayer);

            if (checkWin(board, currentPlayer)) {
                gameWon = true;
                printf("Félicitations, joueur %c a gagné !\n", currentPlayer);
            } else {
                currentPlayer = (currentPlayer == 'X') ? 'O' : 'X'; // Changer de joueur
            }
        } else {
            printf("Mouvement invalide. Réessayez.\n");
        }
    }

    if (!gameWon) {
        printf("Partie terminée. Match nul !\n");
    }

    displayBoard(board);

    return 0;
}




///////////////////////////////////



#include <stdio.h>
#include <stdbool.h>

#define BOARD_SIZE 3
#define MAX_EPISODES 10000
#define LEARNING_RATE 0.1
#define DISCOUNT_FACTOR 0.9
#define EXPLORATION_PROB 0.3

// Structure pour représenter une case du plateau
typedef struct {
    char symbol; // Contient 'X', 'O' ou un caractère vide ('.')
    bool occupied; // Indique si la case est occupée ou non
} Cell;

// Fonction pour initialiser le plateau avec des cases vides
void initializeBoard(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    // ...
}

// Fonction pour afficher le plateau de jeu
void displayBoard(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    // ...
}

// Fonction pour vérifier si le plateau est plein (match nul)
bool isBoardFull(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    // ...
}

// Fonction pour vérifier si un joueur a gagné la partie
bool checkWin(Cell board[BOARD_SIZE][BOARD_SIZE], char player) {
    // ...
}

// Fonction pour placer le symbole d'un joueur sur le plateau
void placeSymbol(Cell board[BOARD_SIZE][BOARD_SIZE], int row, int col, char player) {
    // ...
}

// Fonction pour choisir la meilleure action (numéro de case) en fonction des valeurs Q
int chooseAction(Cell board[BOARD_SIZE][BOARD_SIZE], char player, double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE]) {
    // Exploration aléatoire avec une certaine probabilité
    if ((double)rand() / RAND_MAX < EXPLORATION_PROB) {
        int emptyCells[BOARD_SIZE * BOARD_SIZE];
        int emptyCellCount = 0;
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                if (!board[i][j].occupied) {
                    emptyCells[emptyCellCount] = i * BOARD_SIZE + j;
                    emptyCellCount++;
                }
            }
        }
        return emptyCells[rand() % emptyCellCount];
    } else {
        // Exploitation : choisir la meilleure action selon les valeurs Q
        int maxAction = -1;
        double maxQ = -1;
        for (int i = 0; i < BOARD_SIZE * BOARD_SIZE; i++) {
            if (!board[i / BOARD_SIZE][i % BOARD_SIZE].occupied) {
                if (Q[i][player - 'X'] > maxQ) {
                    maxQ = Q[i][player - 'X'];
                    maxAction = i;
                }
            }
        }
        return maxAction;
    }
}

// Fonction pour entraîner l'IA en utilisant l'algorithme Q-learning
void trainAI() {
    Cell board[BOARD_SIZE][BOARD_SIZE];
    initializeBoard(board);

    double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE] = {0}; // Tableau des valeurs Q

    for (int episode = 0; episode < MAX_EPISODES; episode++) {
        // Initialisation du jeu
        initializeBoard(board);
        char currentPlayer = 'X';
        bool gameWon = false;

        while (!gameWon && !isBoardFull(board)) {
            int action = chooseAction(board, currentPlayer, Q);

            int row = action / BOARD_SIZE;
            int col = action % BOARD_SIZE;

            placeSymbol(board, row, col, currentPlayer);

            if (checkWin(board, currentPlayer)) {
                gameWon = true;
                // Récompense +1 pour la victoire
                Q[action][currentPlayer - 'X'] += LEARNING_RATE * (1 - Q[action][currentPlayer - 'X']);
            } else if (isBoardFull(board)) {
                // Récompense 0 pour un match nul
                Q[action][currentPlayer - 'X'] += LEARNING_RATE * (0 - Q[action][currentPlayer - 'X']);
            } else {
                // Le jeu continue
                char nextPlayer = (currentPlayer == 'X') ? 'O' : 'X';
                int nextAction = chooseAction(board, nextPlayer, Q);
                // Mise à jour de la valeur Q avec l'algorithme Q-learning
                Q[action][currentPlayer - 'X'] += LEARNING_RATE * (DISCOUNT_FACTOR * Q[nextAction][nextPlayer - 'X'] - Q[action][currentPlayer - 'X']);
            }

            currentPlayer = (currentPlayer == 'X') ? 'O' : 'X'; // Changer de joueur
        }
    }

    // Après l'entraînement, vous pouvez enregistrer le tableau de valeurs Q pour une utilisation ultérieure
    // (par exemple, pour jouer contre l'IA entraînée).
}

// Fonction pour permettre à l'IA de jouer contre un joueur humain
void playAgainstAI() {
    // Initialiser le plateau et le tableau de valeurs Q comme précédemment
    // ...

    // Mettre en place la logique de jeu où l'IA joue contre le joueur humain en utilisant Q pour prendre des décisions.
    // ...
}

int main() {
    // Appel de la fonction pour entraîner l'IA
    trainAI();

    // Appel de la fonction pour jouer contre l'IA entraînée
    playAgainstAI();

    return 0;
}




////////////////////////////////////////



#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h> // Pour la fonction rand()

#define BOARD_SIZE 3
#define MAX_EPISODES 10000
#define LEARNING_RATE 0.1
#define DISCOUNT_FACTOR 0.9
#define EXPLORATION_PROB 0.3

// Structure pour représenter une case du plateau
typedef struct {
    char symbol; // Contient 'X', 'O' ou un caractère vide ('.')
    bool occupied; // Indique si la case est occupée ou non
} Cell;

// ... (Implémentation des autres fonctions comme dans la précédente réponse)

// Fonction pour permettre à l'IA de jouer contre un joueur humain
void playAgainstAI(double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE]) {
    Cell board[BOARD_SIZE][BOARD_SIZE];
    initializeBoard(board);

    char currentPlayer = 'X';
    bool gameWon = false;

    printf("Bienvenue dans le jeu Tic-Tac-Toe contre l'IA entraînée !\n");
    printf("Vous jouez en tant que X. Entrez le numéro de ligne (0-2) et le numéro de colonne (0-2) pour placer votre symbole.\n");

    while (!gameWon && !isBoardFull(board)) {
        // Affichage du plateau de jeu actuel
        displayBoard(board);

        if (currentPlayer == 'X') {
            // Tour du joueur humain
            int row, col;
            printf("C'est à votre tour. Entrez le numéro de ligne : ");
            scanf("%d", &row);
            printf("Entrez le numéro de colonne : ");
            scanf("%d", &col);

            // Vérification de la validité du mouvement
            if (row >= 0 && row < BOARD_SIZE && col >= 0 && col < BOARD_SIZE && !board[row][col].occupied) {
                placeSymbol(board, row, col, currentPlayer);

                if (checkWin(board, currentPlayer)) {
                    gameWon = true;
                    printf("Félicitations ! Vous avez gagné contre l'IA.\n");
                } else {
                    currentPlayer = 'O'; // Passage à l'IA pour son tour
                }
            } else {
                printf("Mouvement invalide. Réessayez.\n");
            }
        } else {
            // Tour de l'IA
            int action = chooseAction(board, currentPlayer, Q);

            int row = action / BOARD_SIZE;
            int col = action % BOARD_SIZE;

            printf("L'IA joue : ligne %d, colonne %d\n", row, col);
            placeSymbol(board, row, col, currentPlayer);

            if (checkWin(board, currentPlayer)) {
                gameWon = true;
                printf("Dommage ! L'IA a gagné cette fois.\n");
            } else {
                currentPlayer = 'X'; // Passage au joueur humain pour son tour
            }
        }
    }

    // Affichage du plateau final
    displayBoard(board);

    if (!gameWon) {
        printf("Partie terminée. Match nul !\n");
    }
}

int main() {
    // Appel de la fonction pour entraîner l'IA
    double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE] = {0};
    trainAI(Q);

    // Appel de la fonction pour jouer contre l'IA entraînée
    playAgainstAI(Q);

    return 0;
}




//////////////////////////////////////////



// Fonction de hachage djb2
unsigned long hash(const char *str) {
    unsigned long hash = 5381;
    int c;
    while ((c = *str++))
        hash = ((hash << 5) + hash) + c; // hash * 33 + c
    return hash;
}

// Fonction de discrétisation de l'état
unsigned int discretizeState(const char *state) {
    // Ici, nous utilisons la fonction de hachage djb2 pour obtenir l'indice discret
    unsigned long hashValue = hash(state);
    unsigned int discreteState = hashValue % NUM_DISCRETE_STATES; // NUM_DISCRETE_STATES est la taille du tableau Q discret
    return discreteState;
}




//////////////////////////////////////

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

// Définir NUM_DISCRETE_STATES avec la valeur choisie
#define NUM_DISCRETE_STATES 138 // Utilisez la valeur que vous avez obtenue pour le nombre de classes d'équivalence possibles.

// Structure pour représenter une action (mouvement) du joueur
typedef struct {
    int row;
    int col;
} Move;

// Fonction de hachage djb2
unsigned long hash(const char *str) {
    unsigned long hash = 5381;
    int c;
    while ((c = *str++))
        hash = ((hash << 5) + hash) + c; // hash * 33 + c
    return hash;
}

// Fonction de discrétisation de l'état
unsigned int discretizeState(const char *state) {
    // Convertir l'état du plateau en une représentation canonique
    char canonicalState[10];
    getCanonicalState(state, canonicalState);

    // Discrétiser l'état du plateau
    unsigned long hashValue = hash(canonicalState);
    unsigned int discreteState = hashValue % NUM_DISCRETE_STATES;
    return discreteState;
}

// ... (Le reste du code du jeu Tic-Tac-Toe)

int main() {
    // ... (Initialisation du jeu Tic-Tac-Toe)

    // Boucle principale du jeu
    while (!isGameOver(board)) {
        // ... (Tour du joueur humain et de l'IA)
        // Appeler la fonction de discrétisation pour obtenir l'indice discret
        unsigned int discreteState = discretizeState(board);

        // Maintenant, vous pouvez utiliser discreteState pour accéder à la valeur Q appropriée dans le tableau Q discret.
        // ... (Mise à jour du tableau Q discret et apprentissage par renforcement)

        // ... (Affichage du plateau, vérification de la fin du jeu, etc.)
    }

    // ... (Affichage du résultat final du jeu)

    return 0;
}








////////////////////////////////  mon code /////////////////////////

#include <stdio.h>
#include <stdbool.h>

#define BOARD_SIZE 3

// Structure pour représenter une case du plateau
typedef struct {
    char symbol; // Contient 'X', 'O' ou un caractère vide ('.')
    bool occupied; // Indique si la case est occupée ou non
} Cell;

// Fonction pour initialiser le plateau avec des cases vides
void initializeBoard(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            board[i][j].symbol = '.';
            board[i][j].occupied = false;
        }
    }
}

// Fonction pour afficher le plateau de jeu
void displayBoard(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            printf(" %c ", board[i][j].symbol);
            if (j < BOARD_SIZE - 1)
                printf("|");
        }
        printf("\n");
        if (i < BOARD_SIZE - 1)
            printf("-----------\n");
    }
}

// Fonction pour vérifier si le plateau est plein (match nul)
bool isBoardFull(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            if (!board[i][j].occupied)
                return false;
        }
    }
    return true;
}

// Fonction pour vérifier si un joueur a gagné la partie
bool checkWin(Cell board[BOARD_SIZE][BOARD_SIZE], char player) {
    // Vérification des lignes et des colonnes
    for (int i = 0; i < BOARD_SIZE; i++) {
        if ((board[i][0].symbol == player && board[i][1].symbol == player && board[i][2].symbol == player) ||
            (board[0][i].symbol == player && board[1][i].symbol == player && board[2][i].symbol == player)) {
            return true;
        }
    }
    // Vérification des diagonales
    if ((board[0][0].symbol == player && board[1][1].symbol == player && board[2][2].symbol == player) ||
        (board[0][2].symbol == player && board[1][1].symbol == player && board[2][0].symbol == player)) {
        return true;
    }
    return false;
}

// Fonction pour placer le symbole d'un joueur sur le plateau
void placeSymbol(Cell board[BOARD_SIZE][BOARD_SIZE], int row, int col, char player) {
    board[row][col].symbol = player;
    board[row][col].occupied = true;
}

int main() {
    Cell board[BOARD_SIZE][BOARD_SIZE];
    initializeBoard(board);

    char currentPlayer = 'X';
    int row, col;
    bool gameWon = false;

    printf("Bienvenue dans le jeu Tic-Tac-Toe !\n");

    while (!gameWon && !isBoardFull(board)) {
        displayBoard(board);
        printf("Joueur %c, c'est à votre tour. Entrez le numéro de ligne (0-2) : ", currentPlayer);
        scanf("%d", &row);
        printf("Entrez le numéro de colonne (0-2) : ");
        scanf("%d", &col);

        if (row >= 0 && row < BOARD_SIZE && col >= 0 && col < BOARD_SIZE && !board[row][col].occupied) {
            placeSymbol(board, row, col, currentPlayer);

            if (checkWin(board, currentPlayer)) {
                gameWon = true;
                printf("Félicitations, joueur %c a gagné !\n", currentPlayer);
            } else {
                currentPlayer = (currentPlayer == 'X') ? 'O' : 'X'; // Changer de joueur
            }
        } else {
            printf("Mouvement invalide. Réessayez.\n");
        }
    }

    if (!gameWon) {
        printf("Partie terminée. Match nul !\n");
    }

    displayBoard(board);

    return 0;
}
"
et suite :
"#include <stdio.h>
#include <stdbool.h>

#define BOARD_SIZE 3
#define MAX_EPISODES 10000
#define LEARNING_RATE 0.1
#define DISCOUNT_FACTOR 0.9
#define EXPLORATION_PROB 0.3

// Structure pour représenter une case du plateau
typedef struct {
    char symbol; // Contient 'X', 'O' ou un caractère vide ('.')
    bool occupied; // Indique si la case est occupée ou non
} Cell;

// Fonction pour initialiser le plateau avec des cases vides
void initializeBoard(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    // ...
}

// Fonction pour afficher le plateau de jeu
void displayBoard(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    // ...
}

// Fonction pour vérifier si le plateau est plein (match nul)
bool isBoardFull(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    // ...
}

// Fonction pour vérifier si un joueur a gagné la partie
bool checkWin(Cell board[BOARD_SIZE][BOARD_SIZE], char player) {
    // ...
}

// Fonction pour placer le symbole d'un joueur sur le plateau
void placeSymbol(Cell board[BOARD_SIZE][BOARD_SIZE], int row, int col, char player) {
    // ...
}

// Fonction pour choisir la meilleure action (numéro de case) en fonction des valeurs Q
int chooseAction(Cell board[BOARD_SIZE][BOARD_SIZE], char player, double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE]) {
    // Exploration aléatoire avec une certaine probabilité
    if ((double)rand() / RAND_MAX < EXPLORATION_PROB) {
        int emptyCells[BOARD_SIZE * BOARD_SIZE];
        int emptyCellCount = 0;
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                if (!board[i][j].occupied) {
                    emptyCells[emptyCellCount] = i * BOARD_SIZE + j;
                    emptyCellCount++;
                }
            }
        }
        return emptyCells[rand() % emptyCellCount];
    } else {
        // Exploitation : choisir la meilleure action selon les valeurs Q
        int maxAction = -1;
        double maxQ = -1;
        for (int i = 0; i < BOARD_SIZE * BOARD_SIZE; i++) {
            if (!board[i / BOARD_SIZE][i % BOARD_SIZE].occupied) {
                if (Q[i][player - 'X'] > maxQ) {
                    maxQ = Q[i][player - 'X'];
                    maxAction = i;
                }
            }
        }
        return maxAction;
    }
}

// Fonction pour entraîner l'IA en utilisant l'algorithme Q-learning
void trainAI() {
    Cell board[BOARD_SIZE][BOARD_SIZE];
    initializeBoard(board);

    double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE] = {0}; // Tableau des valeurs Q

    for (int episode = 0; episode < MAX_EPISODES; episode++) {
        // Initialisation du jeu
        initializeBoard(board);
        char currentPlayer = 'X';
        bool gameWon = false;

        while (!gameWon && !isBoardFull(board)) {
            int action = chooseAction(board, currentPlayer, Q);

            int row = action / BOARD_SIZE;
            int col = action % BOARD_SIZE;

            placeSymbol(board, row, col, currentPlayer);

            if (checkWin(board, currentPlayer)) {
                gameWon = true;
                // Récompense +1 pour la victoire
                Q[action][currentPlayer - 'X'] += LEARNING_RATE * (1 - Q[action][currentPlayer - 'X']);
            } else if (isBoardFull(board)) {
                // Récompense 0 pour un match nul
                Q[action][currentPlayer - 'X'] += LEARNING_RATE * (0 - Q[action][currentPlayer - 'X']);
            } else {
                // Le jeu continue
                char nextPlayer = (currentPlayer == 'X') ? 'O' : 'X';
                int nextAction = chooseAction(board, nextPlayer, Q);
                // Mise à jour de la valeur Q avec l'algorithme Q-learning
                Q[action][currentPlayer - 'X'] += LEARNING_RATE * (DISCOUNT_FACTOR * Q[nextAction][nextPlayer - 'X'] - Q[action][currentPlayer - 'X']);
            }

            currentPlayer = (currentPlayer == 'X') ? 'O' : 'X'; // Changer de joueur
        }
    }

    // Après l'entraînement, vous pouvez enregistrer le tableau de valeurs Q pour une utilisation ultérieure
    // (par exemple, pour jouer contre l'IA entraînée).
}

// Fonction pour permettre à l'IA de jouer contre un joueur humain
void playAgainstAI() {
    // Initialiser le plateau et le tableau de valeurs Q comme précédemment
    // ...

    // Mettre en place la logique de jeu où l'IA joue contre le joueur humain en utilisant Q pour prendre des décisions.
    // ...
}

int main() {
    // Appel de la fonction pour entraîner l'IA
    trainAI();

    // Appel de la fonction pour jouer contre l'IA entraînée
    playAgainstAI();

    return 0;
}
"
et suite :
"#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h> // Pour la fonction rand()

#define BOARD_SIZE 3
#define MAX_EPISODES 10000
#define LEARNING_RATE 0.1
#define DISCOUNT_FACTOR 0.9
#define EXPLORATION_PROB 0.3

// Structure pour représenter une case du plateau
typedef struct {
    char symbol; // Contient 'X', 'O' ou un caractère vide ('.')
    bool occupied; // Indique si la case est occupée ou non
} Cell;

// ... (Implémentation des autres fonctions comme dans la précédente réponse)

// Fonction pour permettre à l'IA de jouer contre un joueur humain
void playAgainstAI(double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE]) {
    Cell board[BOARD_SIZE][BOARD_SIZE];
    initializeBoard(board);

    char currentPlayer = 'X';
    bool gameWon = false;

    printf("Bienvenue dans le jeu Tic-Tac-Toe contre l'IA entraînée !\n");
    printf("Vous jouez en tant que X. Entrez le numéro de ligne (0-2) et le numéro de colonne (0-2) pour placer votre symbole.\n");

    while (!gameWon && !isBoardFull(board)) {
        // Affichage du plateau de jeu actuel
        displayBoard(board);

        if (currentPlayer == 'X') {
            // Tour du joueur humain
            int row, col;
            printf("C'est à votre tour. Entrez le numéro de ligne : ");
            scanf("%d", &row);
            printf("Entrez le numéro de colonne : ");
            scanf("%d", &col);

            // Vérification de la validité du mouvement
            if (row >= 0 && row < BOARD_SIZE && col >= 0 && col < BOARD_SIZE && !board[row][col].occupied) {
                placeSymbol(board, row, col, currentPlayer);

                if (checkWin(board, currentPlayer)) {
                    gameWon = true;
                    printf("Félicitations ! Vous avez gagné contre l'IA.\n");
                } else {
                    currentPlayer = 'O'; // Passage à l'IA pour son tour
                }
            } else {
                printf("Mouvement invalide. Réessayez.\n");
            }
        } else {
            // Tour de l'IA
            int action = chooseAction(board, currentPlayer, Q);

            int row = action / BOARD_SIZE;
            int col = action % BOARD_SIZE;

            printf("L'IA joue : ligne %d, colonne %d\n", row, col);
            placeSymbol(board, row, col, currentPlayer);

            if (checkWin(board, currentPlayer)) {
                gameWon = true;
                printf("Dommage ! L'IA a gagné cette fois.\n");
            } else {
                currentPlayer = 'X'; // Passage au joueur humain pour son tour
            }
        }
    }

    // Affichage du plateau final
    displayBoard(board);

    if (!gameWon) {
        printf("Partie terminée. Match nul !\n");
    }
}

int main() {
    // Appel de la fonction pour entraîner l'IA
    double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE] = {0};
    trainAI(Q);

    // Appel de la fonction pour jouer contre l'IA entraînée
    playAgainstAI(Q);

    return 0;
}




/////////////////////////////: mon code /////////////////////////////////////



// ... (Le reste du code de la fonction playAgainstAI() comme précédemment)

while (!gameWon && !isBoardFull(board)) {
    // Affichage du plateau de jeu actuel
    displayBoard(board);

    if (currentPlayer == 'X') {
        // Tour du joueur humain
        int row, col;
        printf("C'est à votre tour. Entrez le numéro de ligne : ");
        scanf("%d", &row);
        printf("Entrez le numéro de colonne : ");
        scanf("%d", &col);

        // Vérification de la validité du mouvement
        if (row >= 0 && row < BOARD_SIZE && col >= 0 && col < BOARD_SIZE && !board[row][col].occupied) {
            placeSymbol(board, row, col, currentPlayer);

            // Discrétisation de l'état du plateau après le coup joué par le joueur humain
            unsigned int discreteState = discretizeState(board);

            if (checkWin(board, currentPlayer)) {
                gameWon = true;
                printf("Félicitations ! Vous avez gagné contre l'IA.\n");
            } else {
                currentPlayer = 'O'; // Passage à l'IA pour son tour
            }
        } else {
            printf("Mouvement invalide. Réessayez.\n");
        }
    } else {
        // Tour de l'IA
        int action = chooseAction(board, currentPlayer, Q);

        int row = action / BOARD_SIZE;
        int col = action % BOARD_SIZE;

        printf("L'IA joue : ligne %d, colonne %d\n", row, col);
        placeSymbol(board, row, col, currentPlayer);

        // Discrétisation de l'état du plateau après le coup joué par l'IA
        unsigned int discreteState = discretizeState(board);

        if (checkWin(board, currentPlayer)) {
            gameWon = true;
            printf("Dommage ! L'IA a gagné cette fois.\n");
        } else {
            currentPlayer = 'X'; // Passage au joueur humain pour son tour
        }
    }
}

// ... (Le reste du code de la fonction playAgainstAI() comme précédemment)



////////////////////////////////////////////////////



#ifndef TICTACTOE_H
#define TICTACTOE_H

#include <stdbool.h>

#define BOARD_SIZE 3

// Structure pour représenter une case du plateau
typedef struct {
    char symbol; // Contient 'X', 'O' ou un caractère vide ('.')
    bool occupied; // Indique si la case est occupée ou non
} Cell;

// Fonction pour initialiser le plateau avec des cases vides
void initializeBoard(Cell board[BOARD_SIZE][BOARD_SIZE]);

// Fonction pour afficher le plateau de jeu
void displayBoard(Cell board[BOARD_SIZE][BOARD_SIZE]);

// Fonction pour vérifier si le plateau est plein (match nul)
bool isBoardFull(Cell board[BOARD_SIZE][BOARD_SIZE]);

// Fonction pour vérifier si un joueur a gagné la partie
bool checkWin(Cell board[BOARD_SIZE][BOARD_SIZE], char player);

// Fonction pour placer le symbole d'un joueur sur le plateau
void placeSymbol(Cell board[BOARD_SIZE][BOARD_SIZE], int row, int col, char player);

#endif // TICTACTOE_H




///////////////////////////////


#include <stdio.h>
#include <stdbool.h>
#include "tictactoe.h"
#include "qlearning.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

// ... (Déclarations et autres fonctions)

// Fonction pour afficher le plateau de jeu en utilisant SDL
void displayBoardSDL(Cell board[BOARD_SIZE][BOARD_SIZE], SDL_Renderer* renderer, TTF_Font* font) {
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255); // Couleur de fond (blanc)
    SDL_RenderClear(renderer);

    // Affichage du plateau
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            SDL_Rect rect = { j * 100, i * 100, 100, 100 };
            SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255); // Couleur des lignes (noir)
            SDL_RenderDrawRect(renderer, &rect);

            if (board[i][j].symbol == 'X') {
                // Afficher "X"
                SDL_Surface* surface = TTF_RenderText_Solid(font, "X", SDL_Color{ 0, 0, 0, 255 });
                SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
                SDL_RenderCopy(renderer, texture, NULL, &rect);
                SDL_DestroyTexture(texture);
                SDL_FreeSurface(surface);
            }
            else if (board[i][j].symbol == 'O') {
                // Afficher "O"
                SDL_Surface* surface = TTF_RenderText_Solid(font, "O", SDL_Color{ 0, 0, 0, 255 });
                SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
                SDL_RenderCopy(renderer, texture, NULL, &rect);
                SDL_DestroyTexture(texture);
                SDL_FreeSurface(surface);
            }
        }
    }

    SDL_RenderPresent(renderer);
}

// Fonction pour permettre à l'IA de jouer contre un joueur humain en utilisant SDL
void playAgainstAI(SDL_Renderer* renderer, TTF_Font* font, double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE]) {
    Cell board[BOARD_SIZE][BOARD_SIZE];
    initializeBoard(board);

    char currentPlayer = 'X';
    bool gameWon = false;

    printf("Bienvenue dans le jeu Tic-Tac-Toe contre l'IA entraînée !\n");
    printf("Vous jouez en tant que X. Entrez le numéro de ligne (0-2) et le numéro de colonne (0-2) pour placer votre symbole.\n");

    while (!gameWon && !isBoardFull(board)) {
        // Affichage du plateau de jeu actuel
        displayBoardSDL(board, renderer, font);

        // ... (Le reste de la logique de jeu reste inchangé comme dans la version précédente)
    }
}

int main() {
    // Initialisation de SDL
    SDL_Init(SDL_INIT_VIDEO);
    TTF_Init();

    // Création de la fenêtre et du renderer pour l'affichage
    SDL_Window* window = SDL_CreateWindow("Tic-Tac-Toe", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 300, 300, SDL_WINDOW_SHOWN);
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    // Chargement de la police pour l'affichage du texte
    TTF_Font* font = TTF_OpenFont("path/to/your/font.ttf", 60); // Remplacez "path/to/your/font.ttf" par le chemin vers votre police

    // Appel de la fonction pour entraîner l'IA
    double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE] = { 0 };
    trainAI(Q);

    // Appel de la fonction pour jouer contre l'IA entraînée
    playAgainstAI(renderer, font, Q);

    // Nettoyage et fermeture de SDL
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    TTF_CloseFont(font);
    TTF_Quit();
    SDL_Quit();

    return 0;
}




///////////////////////////////

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

#define BOARD_SIZE 3
#define MAX_EPISODES 10000
#define LEARNING_RATE 0.1
#define DISCOUNT_FACTOR 0.9
#define EXPLORATION_PROB 0.3

// Structure pour représenter une case du plateau
typedef struct {
    char symbol; // Contient 'X', 'O' ou un caractère vide ('.')
    bool occupied; // Indique si la case est occupée ou non
} Cell;

// Fonction pour initialiser le plateau avec des cases vides
void initializeBoard(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            board[i][j].symbol = '.';
            board[i][j].occupied = false;
        }
    }
}

// Fonction pour afficher le plateau de jeu
void displayBoard(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            printf(" %c ", board[i][j].symbol);
            if (j < BOARD_SIZE - 1)
                printf("|");
        }
        printf("\n");
        if (i < BOARD_SIZE - 1)
            printf("-----------\n");
    }
}

// Fonction pour vérifier si le plateau est plein (match nul)
bool isBoardFull(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            if (!board[i][j].occupied)
                return false;
        }
    }
    return true;
}

// Fonction pour vérifier si un joueur a gagné la partie
bool checkWin(Cell board[BOARD_SIZE][BOARD_SIZE], char player) {
    // Vérification des lignes et des colonnes
    for (int i = 0; i < BOARD_SIZE; i++) {
        if ((board[i][0].symbol == player && board[i][1].symbol == player && board[i][2].symbol == player) ||
            (board[0][i].symbol == player && board[1][i].symbol == player && board[2][i].symbol == player)) {
            return true;
        }
    }
    // Vérification des diagonales
    if ((board[0][0].symbol == player && board[1][1].symbol == player && board[2][2].symbol == player) ||
        (board[0][2].symbol == player && board[1][1].symbol == player && board[2][0].symbol == player)) {
        return true;
    }
    return false;
}

// Fonction pour placer le symbole d'un joueur sur le plateau
void placeSymbol(Cell board[BOARD_SIZE][BOARD_SIZE], int row, int col, char player) {
    board[row][col].symbol = player;
    board[row][col].occupied = true;
}

// Fonction pour choisir la meilleure action (numéro de case) en fonction des valeurs Q
int chooseAction(Cell board[BOARD_SIZE][BOARD_SIZE], char player, double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE]) {
    // Exploration aléatoire avec une certaine probabilité
    if ((double)rand() / RAND_MAX < EXPLORATION_PROB) {
        int emptyCells[BOARD_SIZE * BOARD_SIZE];
        int emptyCellCount = 0;
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                if (!board[i][j].occupied) {
                    emptyCells[emptyCellCount] = i * BOARD_SIZE + j;
                    emptyCellCount++;
                }
            }
        }
        return emptyCells[rand() % emptyCellCount];
    } else {
        // Exploitation : choisir la meilleure action selon les valeurs Q
        int maxAction = -1;
        double maxQ = -1;
        for (int i = 0; i < BOARD_SIZE * BOARD_SIZE; i++) {
            if (!board[i / BOARD_SIZE][i % BOARD_SIZE].occupied) {
                if (Q[i][player - 'X'] > maxQ) {
                    maxQ = Q[i][player - 'X'];
                    maxAction = i;
                }
            }
        }
        return maxAction;
    }
}

// Fonction pour entraîner l'IA en utilisant l'algorithme Q-learning
void trainAI() {
    Cell board[BOARD_SIZE][BOARD_SIZE];
    initializeBoard(board);

    double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE] = {0}; // Tableau des valeurs Q

    // Initialiser la graine aléatoire pour la sélection d'actions aléatoires
    srand(time(NULL));

    for (int episode = 0; episode < MAX_EPISODES; episode++) {
        // Initialisation du jeu
        initializeBoard(board);
        char currentPlayer = 'X';
        bool gameWon = false;

        while (!gameWon && !isBoardFull(board)) {
            int action = chooseAction(board, currentPlayer, Q);

            int row = action / BOARD_SIZE;
            int col = action % BOARD_SIZE;

            placeSymbol(board, row, col, currentPlayer);

            if (checkWin(board, currentPlayer)) {
                gameWon = true;
                // Récompense +1 pour la victoire
                Q[action][currentPlayer - 'X'] += LEARNING_RATE * (1 - Q[action][currentPlayer - 'X']);
            } else if (isBoardFull(board)) {
                // Récompense 0 pour un match nul
                Q[action][currentPlayer - 'X'] += LEARNING_RATE * (0 - Q[action][currentPlayer - 'X']);
            } else {
                // Le jeu continue
                char nextPlayer = (currentPlayer == 'X') ? 'O' : 'X';
                int nextAction = chooseAction(board, nextPlayer, Q);
                // Mise à jour de la valeur Q avec l'algorithme Q-learning
                Q[action][currentPlayer - 'X'] += LEARNING_RATE * (DISCOUNT_FACTOR * Q[nextAction][nextPlayer - 'X'] - Q[action][currentPlayer - 'X']);
            }

            currentPlayer = (currentPlayer == 'X') ? 'O' : 'X'; // Changer de joueur
        }
    }

    // Après l'entraînement, vous pouvez enregistrer le tableau de valeurs Q pour une utilisation ultérieure
    // (par exemple, pour jouer contre l'IA entraînée).
}

// Fonction pour permettre à l'IA de jouer contre un joueur humain
void playAgainstAI(double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE]) {
    Cell board[BOARD_SIZE][BOARD_SIZE];
    initializeBoard(board);

    char currentPlayer = 'X';
    bool gameWon = false;

    printf("Bienvenue dans le jeu Tic-Tac-Toe contre l'IA entraînée !\n");
    printf("Vous jouez en tant que X. Entrez le numéro de ligne (0-2) et le numéro de colonne (0-2) pour placer votre symbole.\n");

    while (!gameWon && !isBoardFull(board)) {
        // Affichage du plateau de jeu actuel
        displayBoard(board);

        if (currentPlayer == 'X') {
            // Tour du joueur humain
            int row, col;
            printf("C'est à votre tour. Entrez le numéro de ligne : ");
            scanf("%d", &row);
            printf("Entrez le numéro de colonne : ");
            scanf("%d", &col);

            // Vérification de la validité du mouvement
            if (row >= 0 && row < BOARD_SIZE && col >= 0 && col < BOARD_SIZE && !board[row][col].occupied) {
                placeSymbol(board, row, col, currentPlayer);

                if (checkWin(board, currentPlayer)) {
                    gameWon = true;
                    printf("Félicitations ! Vous avez gagné contre l'IA.\n");
                } else {
                    currentPlayer = 'O'; // Passage à l'IA pour son tour
                }
            } else {
                printf("Mouvement invalide. Réessayez.\n");
            }
        } else {
            // Tour de l'IA
            int action = chooseAction(board, currentPlayer, Q);

            int row = action / BOARD_SIZE;
            int col = action % BOARD_SIZE;

            printf("L'IA joue : ligne %d, colonne %d\n", row, col);
            placeSymbol(board, row, col, currentPlayer);

            if (checkWin(board, currentPlayer)) {
                gameWon = true;
                printf("Dommage ! L'IA a gagné cette fois.\n");
            } else {
                currentPlayer = 'X'; // Passage au joueur humain pour son tour
            }
        }
    }

    // Affichage du plateau final
    displayBoard(board);

    if (!gameWon) {
        printf("Partie terminée. Match nul !\n");
    }
}

int main() {
    // Appel de la fonction pour entraîner l'IA
    double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE] = {0};
    trainAI(Q);

    // Appel de la fonction pour jouer contre l'IA entraînée
    playAgainstAI(Q);

    return 0;
}





/////////////////////////::: mon code ////////////////////////////

#ifndef TICTACTOE_H
#define TICTACTOE_H

#include <stdbool.h>

#define BOARD_SIZE 3

// Structure pour représenter une case du plateau
typedef struct {
    char symbol; // Contient 'X', 'O' ou un caractère vide ('.')
    bool occupied; // Indique si la case est occupée ou non
} Cell;

// Fonction pour initialiser le plateau avec des cases vides
void initializeBoard(Cell board[BOARD_SIZE][BOARD_SIZE]);

// Fonction pour afficher le plateau de jeu
void displayBoard(Cell board[BOARD_SIZE][BOARD_SIZE]);

// Fonction pour vérifier si le plateau est plein (match nul)
bool isBoardFull(Cell board[BOARD_SIZE][BOARD_SIZE]);

// Fonction pour vérifier si un joueur a gagné la partie
bool checkWin(Cell board[BOARD_SIZE][BOARD_SIZE], char player);

// Fonction pour placer le symbole d'un joueur sur le plateau
void placeSymbol(Cell board[BOARD_SIZE][BOARD_SIZE], int row, int col, char player);

#endif // TICTACTOE_H
tictactoe.c :
c
Copy code
#include "tictactoe.h"
#include <stdio.h>

// Fonction pour initialiser le plateau avec des cases vides
void initializeBoard(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            board[i][j].symbol = '.';
            board[i][j].occupied = false;
        }
    }
}

// Fonction pour afficher le plateau de jeu
void displayBoard(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            printf(" %c ", board[i][j].symbol);
            if (j < BOARD_SIZE - 1)
                printf("|");
        }
        printf("\n");
        if (i < BOARD_SIZE - 1)
            printf("-----------\n");
    }
}

// Fonction pour vérifier si le plateau est plein (match nul)
bool isBoardFull(Cell board[BOARD_SIZE][BOARD_SIZE]) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            if (!board[i][j].occupied)
                return false;
        }
    }
    return true;
}

// Fonction pour vérifier si un joueur a gagné la partie
bool checkWin(Cell board[BOARD_SIZE][BOARD_SIZE], char player) {
    // Vérification des lignes et des colonnes
    for (int i = 0; i < BOARD_SIZE; i++) {
        if ((board[i][0].symbol == player && board[i][1].symbol == player && board[i][2].symbol == player) ||
            (board[0][i].symbol == player && board[1][i].symbol == player && board[2][i].symbol == player)) {
            return true;
        }
    }
    // Vérification des diagonales
    if ((board[0][0].symbol == player && board[1][1].symbol == player && board[2][2].symbol == player) ||
        (board[0][2].symbol == player && board[1][1].symbol == player && board[2][0].symbol == player)) {
        return true;
    }
    return false;
}

// Fonction pour placer le symbole d'un joueur sur le plateau
void placeSymbol(Cell board[BOARD_SIZE][BOARD_SIZE], int row, int col, char player) {
    board[row][col].symbol = player;
    board[row][col].occupied = true;
}
qlearning.h :
c
Copy code
#ifndef QLEARNING_H
#define QLEARNING_H

#include "tictactoe.h"

#define MAX_EPISODES 10000
#define LEARNING_RATE 0.1
#define DISCOUNT_FACTOR 0.9
#define EXPLORATION_PROB 0.3
#define NUM_DISCRETE_STATES 138 // Utilisez la valeur que vous avez obtenue pour le nombre de classes d'équivalence possibles.

// Fonction pour entraîner l'IA en utilisant l'algorithme Q-learning
void trainAI();

// Fonction pour choisir la meilleure action (numéro de case) en fonction des valeurs Q
int chooseAction(Cell board[BOARD_SIZE][BOARD_SIZE], char player, double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE]);

#endif // QLEARNING_H
qlearning.c :
c
Copy code
#include "qlearning.h"
#include <stdio.h>
#include <stdlib.h>

// Fonction pour choisir la meilleure action (numéro de case) en fonction des valeurs Q
int chooseAction(Cell board[BOARD_SIZE][BOARD_SIZE], char player, double Q[BOARD_SIZE * BOARD_SIZE][BOARD_SIZE * BOARD_SIZE]) {
    // ... (Le reste du code de la fonction chooseAction() comme dans la version précédente)
}

// Fonction pour entraîner l'IA en utilisant l'algorithme Q-learning
void trainAI() {
    // ... (Le reste du code de la fonction trainAI() comme dans la version précédente)
}
main.c :
c
Copy code
#include <stdio.h>
#include <stdbool.h>
#include "tictactoe.h"
#include "qlearning.h"

int main() {
    // Appel de la fonction pour entraîner l'IA
    trainAI();

    // Appel de la fonction pour jouer contre l'IA entraînée
    playAgainstAI();

    return 0;
}






///////////////////////////////////////////////////////////



